<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>public-pom</artifactId>
        <version>3.0.4</version>
    </parent>

    <groupId>com.atlassian.bamboo.plugin</groupId>
    <artifactId>agent-smith-wallboard</artifactId>
    <version>5.0.2-SNAPSHOT</version>
    <packaging>atlassian-plugin</packaging>

    <name>Agent Smith Wallboard</name>
    <description>Agent Smith Wallboard plugin for Atlassian Bamboo.</description>
    <url>https://marketplace.atlassian.com/plugins/com.atlassian.bamboo.plugin.agent-smith-wallboard</url>
    <inceptionYear>2011</inceptionYear>
    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <organization>
        <name>Atlassian</name>
        <url>http://www.atlassian.com/</url>
    </organization>
    <developers>
        <developer>
            <id>chebert</id>
            <name>Colin Hebert</name>
            <email>chebert@atlassian.com</email>
        </developer>
    </developers>

    <scm>
        <url>https://bitbucket.org/atlassian/agent-smith-wallboard-plugin</url>
        <connection>scm:git:https://bitbucket.org/atlassian/agent-smith-wallboard-plugin.git</connection>
        <developerConnection>
            scm:git:ssh://git@bitbucket.org/atlassian/agent-smith-wallboard-plugin.git
        </developerConnection>
        <tag>HEAD</tag>
    </scm>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <!-- maven-compiler-plugin config -->
        <maven.compiler.source>6</maven.compiler.source>
        <maven.compiler.target>6</maven.compiler.target>

        <!-- dependencies versions -->
        <bamboo.version>5.5-m1</bamboo.version>
        <slf4j.version>1.6.4</slf4j.version>
        <findbugs.version>1.3.9</findbugs.version>
        <jmockit.version>1.6</jmockit.version>
        <hamcrest.version>1.3</hamcrest.version>
        <testng.version>6.8.7</testng.version>
        <aws-java-sdk.version>1.7.1</aws-java-sdk.version>
        <guava.version>10.0.1</guava.version>
        <spring-beans.version>2.0.7</spring-beans.version>
        <sal-api.version>2.10.11</sal-api.version>
        <atlassian-aws.version>1.0.53</atlassian-aws.version>
        <webwork-compat.version>1.23</webwork-compat.version>

        <!-- maven-bamboo dependencies -->
        <bamboo-graphite.version>1.1.0</bamboo-graphite.version>
        <bamboo-plugin.version>4.2.9</bamboo-plugin.version>
        <bamboo-plugin-test-resources.version>3.2.2-fix-1</bamboo-plugin-test-resources.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.googlecode.jmockit</groupId>
            <artifactId>jmockit</artifactId>
            <version>${jmockit.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-core</artifactId>
            <version>${hamcrest.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <version>${hamcrest.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <version>${testng.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>${slf4j.version}</version>
            <scope>test</scope>
        </dependency>
        <!-- provided dependencies -->
        <dependency>
            <groupId>com.google.code.findbugs</groupId>
            <artifactId>jsr305</artifactId>
            <version>${findbugs.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bamboo.plugin</groupId>
            <artifactId>bamboo-graphite-connector</artifactId>
            <version>${bamboo-graphite.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bamboo</groupId>
            <artifactId>atlassian-bamboo-api</artifactId>
            <version>${bamboo.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bamboo</groupId>
            <artifactId>atlassian-bamboo-core</artifactId>
            <version>${bamboo.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.bamboo</groupId>
            <artifactId>atlassian-bamboo-web</artifactId>
            <version>${bamboo.version}</version>
            <type>jar</type>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.amazonaws</groupId>
            <artifactId>aws-java-sdk</artifactId>
            <version>${aws-java-sdk.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>${guava.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
            <version>${spring-beans.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <version>${sal-api.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.aws</groupId>
            <artifactId>atlassian-aws</artifactId>
            <version>${atlassian-aws.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian</groupId>
            <artifactId>webwork-compat</artifactId>
            <version>${webwork-compat.version}</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-bamboo-plugin</artifactId>
                <version>${bamboo-plugin.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <productVersion>${bamboo.version}</productVersion>
                    <productDataVersion>${bamboo-plugin-test-resources.version}</productDataVersion>
                    <bundledArtifacts>
                        <bundledArtifact>
                            <groupId>com.atlassian.bamboo.plugin</groupId>
                            <artifactId>bamboo-graphite-connector</artifactId>
                            <version>${bamboo-graphite.version}</version>
                        </bundledArtifact>
                    </bundledArtifacts>
                </configuration>
            </plugin>
        </plugins>
    </build>
</project>
