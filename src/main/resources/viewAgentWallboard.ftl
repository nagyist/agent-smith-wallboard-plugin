<html>
<head>
    <title>[@ww.text name='agent.title' /]</title>
${webResourceManager.requireResource("com.atlassian.bamboo.plugin.agent-smith-wallboard:agent-smith-resources")}
${webResourceManager.requiredResources}
    <meta name="decorator" content="none">
</head>
<body>
<div id="container"></div>
<script type="text/javascript">
(function(){
    /*
        Creates the buildeng wallboard using d3, svg, and ajax
    */

    // JSON vars
    var localAgents;
    var remoteAgents;
    var elasticAgents;
    var ec2Instances;
    var ebsVolumes;
    var queue;
    var instanceName
    var currentdate;
    var datetime;

    var srcUrl = "${context}" + "/download/resources/com.atlassian.bamboo.plugin.agent-smith-wallboard:agent-smith-resources/";
    var jsonUrl = "${context}" + "/getAgentDataJson.action";

    // Svg container objects
    var titleInfo;
    var localBarInfo;
    var remoteBarInfo;
    var elasticBarInfo;
    var ec2Info;
    var queueInfo;

    // Timers
    var timerRefresh;
    // Refresh rates < 5 secs bug transitions
    var timerRefreshFreq = ${refreshInterval};
    var timerFlash;
    var timerFlashFreq = 800;

    // Canvas dimensions
    var defaultCanvasWidth = $("#container").width();
    var defaultCanvasHeight = 80;
    var titleCanvasHeight = 100;
    var ec2CanvasHeight = 80;
    var queueCanvasHeight = 800;

    // General Dispaly vars
    var fontFamily = "arial";
    var fontWeight = "bold";

    // If page is blocked with jsBlockUI plugin
    var blocked = false;

    // If user hasn't logged in
    var authIssue = false;

    // EC2 display vars
    var ec2StatusTypes = ["PENDING", "SHUTTING DOWN", "FAILED", "DISCONNECTED"];
    var ec2BackgroundTextSize = 13;
    var ec2Flash = false;
    var ec2Rad = 5;
    var ec2Width = 120;
    var ec2Height = 45;
    var ec2Padding = 10;
    var ec2Offset = 5;
    var ec2Margins = {"x" : 180, "y" : 0};
    var outlineOffset = 2;
    var barHeight = 45;
    var barOffset = defaultCanvasHeight - barHeight;
    var barMargins = {"x" : 180, "y" : 0};
    var agentCountHorizontalOffset = 10;

    // Title display vars
    var refreshRectWidth = (defaultCanvasWidth - ec2Margins['x']*2);
    var refreshBarHeight = 3;
    var ebsHorizontalOffset = 20;

    // Font sizes
    var ec2TextSize = 43;
    var syncTextSize = 15;
    var costTextSize = 30;
    var titleTextSize = 32;
    var labelTextSize = 15;
    var countTextSize = 45;

    // Queue display vars
    var queueFadeTime = 800;
    var queueRad = 3;
    var queueBarHeight = 20;
    var queueBarPadding = 5;
    var queueBarVerticalOffset = 50;
    var queuePlanNameSize = 14;
    var queueStatusWidth = 15;

    // Colours vars
    var colourTitleLabel = "#999";
    var colourTitleRefreshBarInitial = "green";
    var colourTitleRefreshBarFinal = "red";
    var colourTitleLastRefresh = "#333";
    var colourTitleDisconnectedEbs = "#900";

    var colourAgentBarTotal = "#f90";
    var colourAgentBarDisabled = "#333";
    var colourAgentBarIdle = "#36f";
    var colourAgentBarHung = "#900";
    var colourAgentOutline = "black"
    var colourAgentLabels = "#999";
    var colourAgentNameIdle = "#36f";
    var colourAgentNameBusy = "#f90";
    var colourAgentColons = "#666";
    var colourAgentNameDisabled = "#333";
    var colourAgentNameHung = "#900";

    var colourEc2NoduleStarting = "#36f";
    var colourEc2NoduleShutting = "#777";
    var colourEc2NoduleFailed = "#900";
    var colourEc2NoduleDisconnected = "#900";
    var colourEc2NoduleDisconnectedOutline = "#FFF";
    var colourEc2NumText = "#FFF";
    var colourEc2Outline = "#333";
    var colourEc2DescText = "#FFF";
    var colourEc2ElasticCost = "#0A0";

    var colourQueueBarOverdue = "#600";
    var colourQueueBarNormal = "#333";
    var colourQueueLabels = "#999";
    var colourQueueBarMask = "black";
    var colourQueueTime = "#999";
    var colourQueueTitle = "#999";


    // Create SVG elements
    var svgTitle = d3.select("#container")
            .append("svg")
            .attr("width", defaultCanvasWidth)
            .attr("height", titleCanvasHeight)
            .attr("class", "titleCanvas");

    var svgLocal = d3.select("#container")
            .append("svg")
            .attr("width", defaultCanvasWidth)
            .attr("height", defaultCanvasHeight)
            .attr("class", "localAgentCanvas");

    var svgRemote = d3.select("#container")
            .append("svg")
            .attr("width", defaultCanvasWidth)
            .attr("height", defaultCanvasHeight)
            .attr("class", "remoteAgentCanvas");

    var svgElastic = d3.select("#container")
            .append("svg")
            .attr("width", defaultCanvasWidth)
            .attr("height", defaultCanvasHeight)
            .attr("class", "elasticAgentCanvas");

    var svgEc2 = d3.select("#container")
            .append("svg")
            .attr("width", defaultCanvasWidth)
            .attr("height", ec2CanvasHeight)
            .attr("class", "ec2Canvas");

    var svgQueue = d3.select("#container")
            .append("svg")
            .attr("width", defaultCanvasWidth)
            .attr("height", queueCanvasHeight)
            .attr("class", "queueCanvas");

    // Converts a millisecs => HH:MM:SS
    function makeTimeHumanReadable(time) {

        if(time == null){return null;}

        var elapsed = time;
        var unit;
        if(time > 0) {
            elapsed = time/1000;
            if (elapsed > 60) {
                elapsed /= 60;
                if(elapsed > 60) {
                    elapsed /= 60;
                    unit = "h";
                }else{
                    unit = "m";
                }
            }else{
                unit = "s";
            }
        }

        elapsed = Math.round(elapsed);
        return elapsed + unit;
    }

    // Draws title svg elements
    function drawTitle(label, ebsData, margins, canvas){
        var yPos = margins["y"];
        var xPos = margins["x"];
        var showEbs = ebsData[0];

        // Draws title text, e.g. "Agent Status: XBAC"
        var label = canvas.selectAll("text")
                .data([label], function(d){return d;})
                .enter()
                .append('text')
                .attr("y", yPos + titleCanvasHeight - titleTextSize/2)
                .attr("x", xPos)
                .text(function(d) { return d + ": " + instanceName })
                .attr("font-size", titleTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", "start")
                .attr("fill", colourTitleLabel);

        var refreshBar;

//        // Draws refresh bar"
//        var refreshBar = canvas.append('rect')
//                .attr("y", yPos + titleCanvasHeight - titleTextSize/2 - 32)
//                .attr("x", xPos)
//                .attr("height", refreshBarHeight)
//                .attr("width", 0)
//                .attr("fill", colourTitleRefreshBarInitial)
//                .attr("opacity", 0.8)
//                .attr("ry", 1)
//                .attr("rx", 1);
//
//        // Starts refresh bar
//        refreshBar.transition().duration(timerRefreshFreq*2)
//                .attr("width",refreshRectWidth)
//                .attr("fill", colourTitleRefreshBarFinal);

        // Draws last sync time
        var lastRefresh = canvas.append('text')
                .attr("y", yPos + titleCanvasHeight - syncTextSize/2)
                .attr("x", xPos)
                .attr("dy", -45)
                .text(datetime)
                .attr("font-size", syncTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", "start")
                .attr("fill", colourTitleLastRefresh);

        // Draws disconnected ebs volumes
        var ebs = canvas.selectAll()
                .data([ebsData[1]], function(d){return d;})
                .enter()
                .append('text')
                .attr("y", yPos + titleCanvasHeight - titleTextSize/2)
                .attr("x", defaultCanvasWidth - xPos)
                .text(function(d) { return d + " Detached Volumes" })
                .attr("font-size", titleTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", "end")
                .attr("fill", colourTitleDisconnectedEbs)
                .attr("opacity", function(){
                    return showEbs ? 1 : 0;
                });

        return {"title" : label, "ebs" : ebs, "lastRefresh" : lastRefresh, "refreshBar" : refreshBar};
    }

    // Updates title svg elements
    function animateTitle(svgElements, ebsData, margins, canvas){
        var yPos = margins["y"];
        var xPos = margins["x"];
        var showEbs = ebsData[0];

//        // Stops refresh bar transition
//        svgElements.refreshBar.transition().duration(0);
//
//        // Resets refresh bar transition
//        svgElements.refreshBar.transition().duration(1500)
//                .attr("opacity", 0)
//                .attr("fill", colourTitleRefreshBarInitial)
//                .transition().duration(0)
//                .attr("width", 0)
//                .attr("opacity", 0.8);
//
//        // Starts refresh bar
//        svgElements.refreshBar.transition().duration(timerRefreshFreq*2).delay(1700)
//                .attr("width",refreshRectWidth)
//                .attr("fill", colourTitleRefreshBarFinal);

        // Updates disconnected ebs volumes
        svgElements.ebs.data([ebsData[1]])
                .transition().duration(1500)
                .text(function(d) { return d + " Detached Volumes" })
                .attr("opacity", function(){
                    return showEbs ? 1 : 0;
                });

        // Updates last sync data
        svgElements.lastRefresh.transition().duration(1500)
                .text(datetime);

        return svgElements;
    }

    // Draws bar svg elements
    function drawAgentBar(label, dataset, barOffset, margins, canvas){
        var yPos = margins["y"];
        var xPos = margins["x"];
        var total = dataset[0] + dataset[1] + dataset[2] + dataset[3];
        var busy = dataset[0];
        var disabled = dataset[1];
        var idle = dataset[2];
        var hung = dataset[3];
        dataset[0] = total;

        var totalBarWidth = defaultCanvasWidth - 2*margins["x"];

        // Draws agent bars
        var agentBar = canvas.selectAll()
                .data(dataset)
                .enter()
                .append("rect")
                .attr("y", barOffset + yPos)
                .attr("x", function(d,i) {
                    // Total and disabled
                    if(i <= 1){
                        return xPos;
                    // Idle
                    }else if(i == 2){
                        return total == 0 ? xPos : xPos + (disabled)/total * totalBarWidth;
                     // Hung
                    }else{
                        return total == 0 ? xPos : xPos + (total - d)/total * totalBarWidth;
                    }
                })
                .attr("width", function(d,i) {
                    return total == 0 ? 0 : d/total * totalBarWidth;
                })
                .attr("height", barHeight)
                .attr("fill", function(d,i) {
                    // Total
                    if(i == 0){
                        return colourAgentBarTotal;
                    // Disabled
                    }else if(i == 1){
                        return colourAgentBarDisabled;
                    // Idle
                    }else if(i == 2){
                        return colourAgentBarIdle;
                    // Hung
                    }else{
                        return colourAgentBarHung;
                    }
                });

        // Draws rounded corner mask
        var outline = canvas.selectAll()
                .data([total])
                .enter()
                .append("rect")
                .attr("y", barOffset + yPos - outlineOffset/2)
                .attr("x", xPos - outlineOffset/2)
                .attr("width", function(d,i) {
                    return totalBarWidth + outlineOffset;
                })
                .attr("height", barHeight + outlineOffset)
                .attr("stroke-width","10")
                .attr("stroke",colourAgentOutline)
                .attr("fill-opacity", function(d,i) {
                    return "0";
                })
                .attr("opacity", function(d,i) {
                    return total == 0 ? "0" : "1";
                })
                .attr("ry", barHeight/3)
                .attr("rx", barHeight/3);

        // Draws agent titles
        var agentLabel = canvas.selectAll()
                .data([label])
                .enter()
                .append("text")
                .text(function(d){return d;})
                .attr("x", xPos)
                .attr("y", barOffset + yPos - labelTextSize)
                .attr("font-size", labelTextSize)
                .attr("text-anchor", "start")
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("fill", colourAgentLabels);

        // Draws counts (idle and busy)
        var agentCountsIdleBusy = canvas.selectAll()
                .data([idle,busy], function(d, i){
                    return label + ":" + i;
                })
                .enter()
                .append("text")
                .text(function(d){
                    if(d==0){d="";}
                    return d;
                })
                .attr("x", function(d,i){
                    // Varies the distance of the counts from the centre
                    // e.g.
                    //      XX ======= XX
                    // Increasing agentCountHorizontalOffset:
                    // XX      =======      XX
                    return  xPos + i*totalBarWidth + agentCountHorizontalOffset*((2*i) - 1);
                })
                .attr("y", barOffset + yPos)
                .attr("font-size", countTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", function(d,i){
                    if (i == 0) {
                        return "end";
                    } else {
                        return "start";
                    }
                })
                .attr("dy", -6+ countTextSize/2 + barHeight/2)
                .attr("fill", function(d,i) {
                    if(i == 0){
                        return colourAgentNameIdle;
                    }else{
                        return colourAgentNameBusy;
                    }
                })
                .style("opacity", function(d) {
                    return d == 0 ? "0" : "1";
                });

        // Draws colons
        var colons = canvas.selectAll()
                .data([":",":"], function(d, i){
                    return label + ":" + (i+2);
                })
                .enter()
                .append("text")
                .text(function(d){
                    return d;
                })
                .attr("x", function(d,i){
                    // Varies the distance of the counts from the centre.
                    // This depends on the number of chars closer to the bar (outer offset), as well as the initial
                    // horizontal offset
                    var innerCount;
                    if(i == 0){
                        innerCount = idle;
                    }else{
                        innerCount = busy;
                    }

                    var outerOffset = innerCount.toString().length * 26 + 5;
                    if(innerCount == 0){outerOffset = 0;}

                    return  xPos + i*totalBarWidth + (agentCountHorizontalOffset + outerOffset)*((2 * i) - 1);
                })
                .attr("y", barOffset + yPos)
                .attr("font-size", countTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", function(d,i){
                    if (i == 0) {
                        return "end";
                    } else {
                        return "start";
                    }
                })
                .attr("dy",  -8 + countTextSize/2 + barHeight/2)
                .attr("fill", function(d,i) {
                    return colourAgentColons;
                })
                .style("opacity", function(d,i) {
                    var retval = 0;
                    if(i == 0){
                        if(idle != 0 && disabled != 0){retval = 1;}
                    }else{
                        if(busy != 0 && hung != 0){retval = 1;}
                    }
                    return retval;
                });


        // Draws counts (disabled and hung)
        var agentCountsDisabledHung = canvas.selectAll()
                .data([disabled,hung], function(d, i){
                    return label + ":" + (i+2);
                })
                .enter()
                .append("text")
                .text(function(d){
                    if(d==0){d="";}
                    return d;
                })
                .attr("x", function(d,i){
                    var innerCount;
                    if( i == 0){
                        innerCount = idle;
                    }else{
                        innerCount = busy;
                    }

                    var outerOffset = innerCount.toString().length * 28 + 25;
                    if(innerCount == 0){outerOffset = 0;}
                    return  xPos + i*totalBarWidth + (agentCountHorizontalOffset + outerOffset)*((2 * i) - 1);
                })
                .attr("y", barOffset + yPos)
                .attr("font-size", countTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", function(d,i){
                    if (i == 0) {
                        return "end";
                    } else {
                        return "start";
                    }
                })
                .attr("dy", -6+ countTextSize/2 + barHeight/2)
                .attr("fill", function(d,i) {
                    if(i == 0){
                        return colourAgentNameDisabled;
                    }else{
                        return colourAgentNameHung;
                    }
                })
                .style("opacity", function(d) {
                    return d == 0 ? "0" : "1";
                });

        // Sets canvas size depending on whether any instances are available
        canvas.attr("height",function(){
            return total == 0 ? 0 : defaultCanvasHeight;
        });

        return {"bar" : agentBar, "agentCountsIdleBusy" : agentCountsIdleBusy, "agentCountsDisabledHung" : agentCountsDisabledHung, "outline" : outline, "colons" : colons};
    }

    // Updates agent svg elements
    function animateAgentBar(svgElements, dataset, barOffset, margins, canvas) {
        var yPos = margins["y"];
        var xPos = margins["x"];
        var total = dataset[0] + dataset[1] + dataset[2] + dataset[3];
        var busy = dataset[0];
        var disabled = dataset[1];
        var idle = dataset[2];
        var hung = dataset[3];
        var totalBarWidth = defaultCanvasWidth - 2*margins["x"];
        dataset[0] = total;

        // Updates agent bars
        svgElements.bar.data(dataset)
                .transition().duration(1500).delay(1200)
                .attr("x", function(d,i) {
                    // Total and disabled
                    if(i <= 1){
                        return xPos;
                        // Idle
                    }else if(i == 2){
                        return total == 0 ? xPos : xPos + (disabled)/total * totalBarWidth;
                        // Hung
                    }else{
                        return total == 0 ? xPos : xPos + (total - d)/total * totalBarWidth;
                    }
                })
                .attr("width", function(d,i) {
                    return total == 0 ? 0 : d/total * totalBarWidth;
                })
                .style("display",function(){
                    if(total == 0){return "none";}
                });

        // Updates rounded corner mask
        svgElements.outline.data([total])
                .transition().duration(1500)
                .attr("opacity", function(d,i) {
                    return total == 0 ? "0" : "1";
                });

        // Updates counts (idle and busy)
        svgElements.agentCountsIdleBusy.data([idle,busy])
                .transition().duration(1500).delay(1200)
                .text(function(d){
                    if(d == 0){d = "";}
                    return d;
                })
                .style("opacity", function(d) {
                    return d == 0 ? "0" : "1";
                });

        svgElements.colons.data([':',':'])
                .transition().duration(1500).delay(1200)
                .attr("x", function(d,i){
                    var innerCount;
                    if(i == 0){
                        innerCount = idle;
                    }else{
                        innerCount = busy;
                    }

                    var outerOffset = innerCount.toString().length * 26 + 5;
                    if(innerCount == 0){outerOffset = 0;}

                    return  xPos + i*totalBarWidth + (agentCountHorizontalOffset + outerOffset)*((2 * i) - 1);
                })
                .style("opacity", function(d,i) {
                    var retval = 0;
                    if(i == 0){
                        if(idle != 0 && disabled != 0){retval = 1;}
                    }else{
                        if(busy != 0 && hung != 0){retval = 1;}
                    }
                    return retval;
                });


        // Updates counts (disabled and hung)
        svgElements.agentCountsDisabledHung.data([disabled,hung])
                .transition().duration(1500).delay(1200)
                .text(function(d){
                    if(d == 0){d = "";}
                    return d;
                })
                .attr("x", function(d,i){
                        var innerCount;
                        if( i == 0){
                            innerCount = idle;
                        }else{
                            innerCount = busy;
                        }

                        var outerOffset = innerCount.toString().length * 28 + 25;
                        if(innerCount == 0){outerOffset = 0;}
                        return  xPos + i*totalBarWidth + (agentCountHorizontalOffset + outerOffset)*((2 * i) - 1);
                })
                .style("opacity", function(d) {
                    return d == 0 ? "0" : "1";
                });

        // Updates canvas size depending on whether any instances are available
        canvas.transition().duration(1500).delay(0)
                .attr("height",function(){
                    return total == 0 ? 0 : defaultCanvasHeight;
                });

        return svgElements;
    }

    // Draws ec2 svg elements
    function drawEc2Status(dataset, totalElasticAgents, offset, margins, canvas){
        var yPos = margins["y"];
        var xPos = margins["x"];
        var total = dataset[0] + dataset[1] + dataset[2] + dataset[3];
        var cost = "$" + parseFloat(Math.round(dataset.pop()*100)/100).toFixed(2) + " p/h";
        var drawnNodules = 0;

        // Draws ec2 nodules (the coloured lozenges)
        var ec2Nodule = canvas.selectAll("rect")
                .data(dataset,function(d,i){return i;})
                .enter()
                .append("rect")
                .attr("y", offset + yPos - 3)
                .attr("x", function(d,i) {
                    if(d == 0){
                        return xPos + (i - 1) * (ec2Padding + ec2Width);
                    }else{
                        drawnNodules++;
                        return xPos + (drawnNodules - 1) * (ec2Padding + ec2Width);
                    }
                })
                .attr("ry", ec2Rad)
                .attr("rx", ec2Rad)
                .attr("width", function(d,i) {
                    return ec2Width;
                })
                .attr("height", ec2Height)
                .attr("fill", function(d,i) {
                    if(i == 0){
                        // Starting
                        return colourEc2NoduleStarting;
                    }else if(i == 1){
                        // Shutting down
                        return colourEc2NoduleShutting;
                    }else if(i == 2){
                        // Failed
                        return colourEc2NoduleFailed;
                    }else{
                        // Disconnected
                        return colourEc2NoduleDisconnected;
                    }
                })
                .attr("stroke-width", function(d,i) {
                    if(i == 3){
                        return "2";
                    }
                })
                .attr("stroke", function(d,i) {
                    if(i == 3){
                        return colourEc2NoduleDisconnectedOutline;
                    }
                })
                .attr("fill-opacity", function(d,i) {
                    return "1";
                })
                .attr("opacity", function(d,i) {
                    return d == 0 ? "0" : "1";
                });

        drawnNodules = 0;

        // Draws EC2 numbers
        var ec2Text = canvas.selectAll("text")
                .data(dataset, function(d, i){return d + "_" + i;})
                .enter()
                .append("text")
                .text(function(d){return d;})
                .attr("y", offset + yPos + ec2Height/2 )
                .attr("dy", 13 )
                .attr("x", function(d,i) {
                    if(d == 0){
                        return xPos + (i - 1) * (ec2Padding + ec2Width) + ec2Width/2;
                    }else{
                        drawnNodules++;
                        return xPos + (drawnNodules - 1) * (ec2Padding + ec2Width) + ec2Width/2;
                    }
                })
                .attr("stroke-width", 2)
                .attr("stroke", colourEc2Outline)
                .attr("font-size", ec2TextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", "middle")
                .attr("fill", function(d,i) {
                    return colourEc2NumText;
                })
                .attr("opacity", function(d,i) {
                    return d == 0 ? "0" : "1";
                });

        drawnNodules = 0;

        // Draws EC2 types
        var backgroundText = canvas.selectAll("text")
                .data(ec2StatusTypes, function(d){return d;})
                .enter()
                .append("text")
                .text(function(d){return d;})
                .attr("y", offset + yPos)
                .attr("x", function(d,i) {
                    if(dataset[i] == 0){
                        return xPos + (i - 1) * (ec2Padding + ec2Width);
                    }else{
                        drawnNodules++;
                        return xPos + (drawnNodules - 1) * (ec2Padding + ec2Width);
                    }
                })
                .attr("dy", ec2Height + 10)
                .attr("dx", ec2Width - 2)
                .attr("font-size", ec2BackgroundTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", "end")
                .attr("fill", function(d,i) {
                    return colourEc2DescText;
                })
                .attr("opacity", function(d,i) {
                    return dataset[i] == 0 ? "0" : "0.8";
                });

        // Draws EC2 elastic cost
        var costText = canvas.append("text")
                .text(cost)
                .attr("y", offset + yPos + ec2Height/2)
                .attr("dy", 3)
                .attr("x", defaultCanvasWidth - xPos)
                .attr("font-size", costTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", "end")
                .attr("fill", function(d,i) {
                    return colourEc2ElasticCost;
                })
                .attr("opacity", function(d,i) {
                    return d == 0 ? "0" : "1";
                });

        // Updates canvas size depending on whether any ELASTIC instances are available
        canvas.attr("height",function(){
            return totalElasticAgents == 0 ? 0 : defaultCanvasHeight;
        });

        return {"ec2Nodule" : ec2Nodule, "ec2Text" : ec2Text, "cost" : costText, "backgroundText" : backgroundText};
    }

    // Updates EC2 svg elements
    function animateEc2Status(svgElements, dataset, totalElasticAgents, offset, margins, canvas){
        var yPos = margins["y"];
        var xPos = margins["x"];
        var total = dataset[0] + dataset[1] + dataset[2] + dataset[3];
        var cost = "$" + parseFloat(Math.round(dataset.pop()*100)/100).toFixed(2) + " p/h";
        var drawnNodules = 0;

        // Updates the EC2 types
        svgElements.ec2Text.data(dataset)
                .transition().duration(1500).delay(1200)
                .attr("opacity", function(d,i) {
                    return d == 0 ? "0" : "1";
                })
                .attr("x", function(d,i) {
                    if(d == 0){
                        return xPos + (i - 1) * (ec2Padding + ec2Width) + ec2Width/2;
                    }else{
                        drawnNodules++;
                        return xPos + (drawnNodules - 1) * (ec2Padding + ec2Width) + ec2Width/2;
                    }
                })
                .text(function(d){return d;});

        drawnNodules = 0;

        // Updates the EC2 desc
        svgElements.backgroundText.data(ec2StatusTypes)
                .transition().duration(1500).delay(1200)
                .attr("opacity", function(d,i) {
                    return dataset[i] == 0 ? "0" : "0.8";
                })
                .attr("x", function(d,i) {
                    if(dataset[i] == 0){
                        return xPos + (i - 1) * (ec2Padding + ec2Width);
                    }else{
                        drawnNodules++;
                        return xPos + (drawnNodules - 1) * (ec2Padding + ec2Width);
                    }
                })
                .text(function(d){return d;});

        drawnNodules = 0;

        // Updates the EC2 nodules
        svgElements.ec2Nodule.data(dataset)
                .transition().duration(1500).delay(1200)
                .attr("opacity", function(d,i) {
                    return d == 0 ? "0" : "1";
                })
                .attr("x", function(d,i) {
                    if(d == 0){
                        return xPos + (i - 1) * (ec2Padding + ec2Width);
                    }else{
                        drawnNodules++;
                        return xPos + (drawnNodules - 1) * (ec2Padding + ec2Width);
                    }
                })
                .text(function(d){return d;});


        // Updates the EC2 elastic cost
        svgElements.cost.transition().duration(1500).delay(1200)
                .text(cost);

        // Updates canvas size depending on whether any ELASTIC instances are available
        canvas.transition().duration(1500).delay(0)
                .attr("height",function(){
                    return totalElasticAgents == 0 ? 0 : defaultCanvasHeight;
                });

        // Starts disconnected/hanging ec2 nodule flashing
        setTimeout(startFlash,2000);

        return svgElements;
    }

    // Single flash for disconnected/hanging ec2 nodules
    function flashEc2Nodule(svgElements) {
        if( svgElements !== undefined){
            svgElements.ec2Nodule
                    .transition().duration(function(d,i){
                        if(i > 1 && d != 0){return timerFlashFreq/2;}
                    })
                    .attr("fill-opacity","0.5")
                    .transition().duration(function(d,i){
                        if(i > 1 && d != 0){return timerFlashFreq/2;}
                    })
                    .attr("fill-opacity","1.0");
        }
    }

    // Draws queue svg elements
    function drawQueue(dataset, margins, canvas) {
        var yPos = margins["y"] + queueBarVerticalOffset;
        var xPos = margins["x"];
        var totalBarWidth = defaultCanvasWidth - 2*margins["x"];
        var largestTime = d3.max(dataset, function(d){return d.time;});
        var queueScale = d3.scale.linear().domain([0, largestTime]).range([0, totalBarWidth]);
        var numQueueItems = dataset.length;

        if(numQueueItems == 0){return;}

        // Draws queue bars
        var queue = canvas.selectAll("rect")
                .data(dataset, function(d,i){return d.plan + ": " + i;})
                .enter()
                .append("rect")
                .attr("y", function(d,i){
                    return yPos + i*(queueBarHeight + queueBarPadding);
                })
                .attr("x", xPos)
                .attr("ry", queueRad)
                .attr("rx", queueRad)
                .attr("width", function(d,i) {
                    return queueScale(d.time);
                })
                .attr("height", queueBarHeight)
                .attr("fill", function(d,i) {
                    updateOverdue(d);
                    if(d.overdue) {
                        return colourQueueBarOverdue;
                    } else {
                        return colourQueueBarNormal;
                    }
                })
                .style("opacity", 0)
                .transition().duration(queueFadeTime)
                .style("opacity", 1);

        // Draw queue text path (allows for text cropping/stop overflow)
        var paths = canvas.selectAll("path")
                .data(dataset, function(d,i){return d.plan + ": " + i;})
                .enter()
                .append("path")
                .attr("id", function(d,i){
                    return "path" + i;
                })
                .attr("d", function(d,i){
                    var x = queueBarHeight + xPos + agentCountHorizontalOffset;
                    var y = yPos + i*(queueBarHeight + queueBarPadding) + 6 + queueBarHeight/2;

                    // Use the return line below to disable overflow of text on red queue rectangles
                    // return "M" + x + " " + y + " Q" + (x + queueScale(d.time) - 18) + "," + y + " " + (x + queueScale(d.time) - 18) + "," + y + "";

                    // Draw straight textpath arc to allow for text cropping (doesnt work with horizontal lines)
                    return "M" + x + " " + y + " Q" + (x + totalBarWidth - 18) + "," + y + " " + (x + totalBarWidth - 18) + "," + y + "";
                });

        // Adds status icons
        var status = canvas.selectAll()
                .data(dataset, function(d,i){return d.plan + ": " + i;})
                .enter()
                .append("image")
                .attr("xlink:href", function(d){
                    if(d.status == "error"){
                        return srcUrl + "error.png";
                    }else if(d.status == "error_elastic"){
                        return srcUrl + "error_elastic.gif";
                    }else{
                        return srcUrl + "waiting.png";
                    }
                })
                .attr("y", function(d,i){
                    return yPos + i*(queueBarHeight + queueBarPadding) + queueStatusWidth/4;
                })
                .attr("x", xPos + agentCountHorizontalOffset - queueStatusWidth/4)
                .attr("width", queueStatusWidth)
                .attr("height", queueStatusWidth)
                .style("opacity", 0)
                .transition().duration(queueFadeTime)
                .style("opacity",function(d,i){
                    return d.overdue ? 1 : 0.6
                });


        // Append queue text to path
        var textLabel = canvas.selectAll("text")
                .data(dataset, function(d,i){return d.plan + ": " + i;})
                .enter()
                .append("a")
                .attr("xlink:href", function(d) {
                    return "browse/" + d.key  + "/";
                })
                .append("text")
                .attr("font-size", queuePlanNameSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .style("fill-opacity", 0)
                .append("textPath")
                .attr("xlink:href",function(d,i){
                    return "#path" + i;
                })
                .attr("fill", colourQueueLabels)
                .text(function(d) {
                    return d.plan; //d.overdue ? d.plan : "";
                })
                .transition().duration(queueFadeTime)
                .style("fill-opacity", function(d,i){
                    return d.overdue ? 1 : 0.6
                })

        // Draw grey rectangle mask
        var mask = canvas.selectAll()
                .data(dataset, function(d,i){ return "offset" + d.plan + ": " + i;})
                .enter()
                .append("rect")
                .attr("y", function(d,i){
                    return yPos + i*(queueBarHeight + queueBarPadding);
                })
                .attr("x", function(d,i){
                    return xPos + queueScale(d.time);
                })
                .attr("width", function(d,i) {
                    return totalBarWidth - queueScale(d.time);
                })
                .attr("height", queueBarHeight)
                .attr("fill", colourQueueBarMask)
                .style("opacity", 0.6);

        // Draw queue time
        var time = canvas.selectAll()
                .data([largestTime], function(d) {return d})
                .enter()
                .append("text")
                .attr("y", function(d,i){
                    return yPos + i*(queueBarHeight + queueBarPadding);
                })
                .attr("x", function(d,i) {
                    return xPos + totalBarWidth + agentCountHorizontalOffset;
                })
                .attr("fill", colourQueueTime)
                .attr("font-size", 20)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", "start")
                .attr("dy", 7 + queueBarHeight/2)
                .text(function(d){return makeTimeHumanReadable(d);})
                .style("opacity", 0)
                .transition().duration(queueFadeTime)
                .style("opacity", 1);

        // Draw queue build title, e.g. "7 Queue Builds"
        var title = canvas.selectAll()
                .data([numQueueItems], function(d) {return d})
                .enter()
                .append("text")
                .attr("y", yPos - titleTextSize/2)
                .attr("x", xPos)
                .attr("fill", colourQueueTitle)
                .attr("font-size", titleTextSize)
                .attr("font-family", fontFamily)
                .attr("font-weight", fontWeight)
                .attr("text-anchor", "start")
                .text(function(d){
                    if(d == 0){
                        return "";
                    }else if(d == 1) {
                        return d + " Queued Build";
                    }else{
                        return d + " Queued Builds";
                    }
                });

        // No need to adjust canvas height since queue canvas is at the bottom of the page.
        // Add canvas height transition here if adding more gadgets/canvas'

        return {"queue" : queue, "time" : time, "title" : title, "paths" : paths, "status" : status };
    }

    // Updates queue svg elements
    function animateQueue(svgElements, dataset, margins, canvas) {

        canvas.selectAll("a").remove();
        canvas.selectAll("rect").remove();
        canvas.selectAll("text").remove();
        canvas.selectAll("path").remove();
        canvas.selectAll("image").remove();

        return drawQueue(dataset,margins,canvas);
    }

    // Gets json data + Updates svg elements
    function reDraw() {
        // Stops disconnect/hanging ec2 instance flash
        stopFlash();

        // Subsequent json requests
        d3.json(jsonUrl, function(e,data){
            authIssue = false;

            if(e == null){
                // Unblocks screen
                unblockScreen();
                // Updates global vars
                getData(data);

                // Updates svg elements
                titleInfo  = animateTitle(titleInfo,ebsVolumes,barMargins,svgTitle);
                localBarInfo = animateAgentBar(localBarInfo, localAgents, barOffset, barMargins, svgLocal);
                remoteBarInfo = animateAgentBar(remoteBarInfo, remoteAgents, barOffset, barMargins, svgRemote);
                elasticBarInfo = animateAgentBar(elasticBarInfo, elasticAgents, barOffset, barMargins, svgElastic);
                ec2Info = animateEc2Status(ec2Info,ec2Instances,elasticAgents[0], ec2Offset, ec2Margins, svgEc2);
                queueInfo = animateQueue(queueInfo,queue,barMargins,svgQueue);
            }else{
                // General error
                if(blocked == false){
                    blockScreen("Oops, something went wrong.");
                }
            }
        });

        // Auth error
        if(authIssue == true && blocked == false){
            blockScreen("Bugger, you've been logged out.");
        }

        authIssue = true;
    }

    // Initial Json Request
    d3.json(jsonUrl, function(data){

        // Updates global vars
        getData(data);

        // Updates svg elements
        titleInfo  = drawTitle('Agent Status',ebsVolumes,barMargins,svgTitle);
        localBarInfo = drawAgentBar('Local Agents', localAgents, barOffset, barMargins, svgLocal);
        remoteBarInfo = drawAgentBar('Remote Agents', remoteAgents, barOffset, barMargins, svgRemote);
        elasticBarInfo = drawAgentBar('Elastic Agents', elasticAgents, barOffset, barMargins, svgElastic);
        ec2Info = drawEc2Status(ec2Instances,elasticAgents[0], ec2Offset, ec2Margins, svgEc2);
        queueInfo = drawQueue(queue, barMargins, svgQueue);

    });

    function blockScreen(message){
        blocked = true;
        $.blockUI({
            message: '<img src=' + srcUrl + 'logo.png><h2>' + message + '</h2>',
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#ddd',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .8,
                color: '#444'
            }
        });
    }

    function unblockScreen(){
        blocked = false;
        $.unblockUI();
    }

    // Sets global vars from json data
    function getData(data){
        localAgents = data.local;
        remoteAgents = data.remote;
        elasticAgents = data.elastic;
        ec2Instances = data.ec2;
        ebsVolumes = data.ebs;
        queue = data.queue;
        instanceName = data.name;
        currentdate = new Date();
        datetime = "Last Sync: " + fixDateDisplay(currentdate.getDate()) + "/"
                + fixDateDisplay((currentdate.getMonth()+1))  + "/"
                + currentdate.getFullYear() + " "
                + fixDateDisplay(currentdate.getHours()) + ":"
                + fixDateDisplay(currentdate.getMinutes()) + ":"
                + fixDateDisplay(currentdate.getSeconds());
    }

    function updateOverdue(d){
        d.overdue = (d.time > ${queueOverdueThreshold});
        return d;
    }

    // Fixes time display
    function fixDateDisplay(time){
        return (time < 10) ? ("0" + time) : time;
    }

    // Fixes time display
    function startLoop() {
        timerRefresh = setInterval(reDraw, timerRefreshFreq);
        startFlash();
    }

    // Flashes disconnected/hanging elastic instances dependant on data
    function flash(){
        flashEc2Nodule(ec2Info);
    }

    // Stops flashing
    function stopFlash() {
        clearInterval(timerFlash);
    }

    // Starts flash timer
    function startFlash() {
        timerFlash = setInterval(flash, timerFlashFreq);
    }

    // Starts refresh timer
    startLoop();

})();
</script>
</body>
</html>
