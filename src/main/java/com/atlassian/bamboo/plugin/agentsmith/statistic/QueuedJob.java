package com.atlassian.bamboo.plugin.agentsmith.statistic;

import java.util.Date;

/**
 * Job in the bamboo queue.
 */
public class QueuedJob {
    private final String name;
    private final String key;
    private final Date queuedTime;
    private AgentAvailability agentAvailability;

    /**
     * Creates a job based on its name and the time at which it was queued.
     *
     * @param name       name of the job.
     * @param queuedTime time at which the job was queued.
     */
    public QueuedJob(String name, String key, Date queuedTime) {
        this.name = name;
        this.key = key;
        this.queuedTime = queuedTime;
    }

    public String getName() {
        return name;
    }

    public String getKey() {
        return key;
    }

    public Date getQueuedTime() {
        return queuedTime;
    }

    public AgentAvailability getAgentAvailability() {
        return agentAvailability;
    }

    public void setAgentAvailability(AgentAvailability agentAvailability) {
        this.agentAvailability = agentAvailability;
    }

    /**
     * Availability of agents to run this job.
     */
    public static enum AgentAvailability {
        /**
         * Executable on a local or remote agent.
         */
        EXECUTABLE,
        /**
         * Awaiting for an elastic agent to be set up.
         */
        ELASTIC,
        /**
         * No agents available.
         */
        NONE
    }
}
