package com.atlassian.bamboo.plugin.agentsmith.wallboard;

import com.atlassian.bamboo.plugin.agentsmith.AgentSmithService;
import com.atlassian.bamboo.plugin.agentsmith.statistic.ElasticProperties;
import com.atlassian.bamboo.plugin.agentsmith.statistic.QueuedJob;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;
import com.opensymphony.webwork.dispatcher.json.JSONArray;
import com.opensymphony.webwork.dispatcher.json.JSONException;
import com.opensymphony.webwork.dispatcher.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Status;
import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Type;

/**
 * Wallboard action in charge of converting the {@link Statistics} element into Json content.
 */
public class AgentSmithWallboardActionImpl extends AgentSmithWallboardAction {
    private static final Logger logger = LoggerFactory.getLogger(AgentSmithWallboardActionImpl.class);
    private static final String QUEUE_OVERDUE = "overdue";
    private static final String QUEUE_PLAN = "plan";
    private static final String QUEUE_KEY = "key";
    private static final String QUEUE_TIME = "time";
    private static final String QUEUE_STATUS = "status";
    private static final String QUEUE = "queue";
    private static final String EBS = "ebs";
    private static final String EC2 = "ec2";
    private static final long DEFAULT_DETACHED_VOLUMES_THRESHOLD = 50;
    private long detachedVolumesThreshold = DEFAULT_DETACHED_VOLUMES_THRESHOLD;

    /**
     * Creates a Wallboard action.
     *
     * @param agentSmithService service in charge of generating statistics.
     */
    public AgentSmithWallboardActionImpl(AgentSmithService agentSmithService) {
        super(agentSmithService);
    }

    /**
     * Gets the name used to identify an agent type.
     *
     * @param agentType agent type to change into a String.
     * @return the string value of the agent type.
     */
    private static String getAgentTypeName(Type agentType) {
        switch (agentType) {
            case LOCAL:
                return "local";
            case REMOTE:
                return "remote";
            case ELASTIC:
                return "elastic";
            default:
                logger.error("Couldn't get the String version of '{}'", agentType);
                return "unknown";
        }
    }

    /**
     * Gets the name used to identify the agent availability for a queued job.
     *
     * @param agentAvailability agent availability to change into a String.
     * @return the string value of the agent availability.
     */
    private static String getAgentAvailabilityName(QueuedJob.AgentAvailability agentAvailability) {
        switch (agentAvailability) {
            case EXECUTABLE:
                return "waiting";
            case ELASTIC:
                return "error_elastic";
            case NONE:
                return "error";
            default:
                logger.error("Couldn't get the String version of '{}'", agentAvailability);
                return "unknown";
        }
    }

    /**
     * Gets the threshold for detached volumes.
     *
     * @return the threshold for detached volumes.
     */
    @SuppressWarnings("unused")
    public long getDetachedVolumeThreshold() {
        return detachedVolumesThreshold;
    }

    /**
     * Sets the threshold for detached volumes.
     *
     * @param detachedVolumesThreshold threshold for detached volumes
     */
    @SuppressWarnings("unused")
    public void setDetachedVolumesThreshold(long detachedVolumesThreshold) {
        this.detachedVolumesThreshold = detachedVolumesThreshold;
    }

    @Override
    public JSONObject convertStatistics(Statistics statistics) throws JSONException {
        JSONObject jsonObject = new JSONObject();

        addAgentsDetails(jsonObject, statistics);
        addQueueDetails(jsonObject, statistics);
        addElasticDetails(jsonObject, statistics);
        addEbsDetails(jsonObject, statistics);

        jsonObject.put("name", getBamboo().getAdministrationConfiguration().getInstanceName());

        return jsonObject;
    }

    private void addAgentsDetails(JSONObject parentObject, Statistics statistics) throws JSONException {
        Map<Type, Statistics.Count<Status>> statusCountPerAgentType = statistics.getStatusCountPerAgentType();
        for (Map.Entry<Type, Statistics.Count<Status>> entry : statusCountPerAgentType.entrySet()) {
            String agentType = getAgentTypeName(entry.getKey());
            Statistics.Count<Status> count = entry.getValue();

            //The result is an array with in order busy, disabled, idle, hung
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(count.get(Status.BUSY));
            jsonArray.put(count.get(Status.DISABLED));
            jsonArray.put(count.get(Status.IDLE));
            jsonArray.put(count.get(Status.HUNG));

            parentObject.put(agentType, jsonArray);
        }
    }

    private void addQueueDetails(JSONObject parentObject, Statistics statistics) throws JSONException {
        JSONArray queue = new JSONArray();
        long currentTimestamp = System.currentTimeMillis();

        for (QueuedJob queuedJob : statistics.getQueuedJobs()) {
            JSONObject queueDetails = new JSONObject();

            queueDetails.put(QUEUE_OVERDUE, isOverdue(queuedJob));
            queueDetails.put(QUEUE_PLAN, queuedJob.getName());
            queueDetails.put(QUEUE_KEY, queuedJob.getKey());
            queueDetails.put(QUEUE_TIME, currentTimestamp - queuedJob.getQueuedTime().getTime());
            queueDetails.put(QUEUE_STATUS, getAgentAvailabilityName(queuedJob.getAgentAvailability()));

            queue.put(queueDetails);
        }
        parentObject.put(QUEUE, queue);
    }

    private void addElasticDetails(JSONObject parentObject, Statistics statistics) throws JSONException {
        //The result is an array with in order pending, shutting, failed, offline, totalCost
        JSONArray jsonArray = new JSONArray();

        Statistics.Count<ElasticProperties.ElasticStatus> count = statistics.getElasticStatusCount();
        jsonArray.put(count.get(ElasticProperties.ElasticStatus.PENDING));
        jsonArray.put(count.get(ElasticProperties.ElasticStatus.SHUTTING_DOWN));
        jsonArray.put(count.get(ElasticProperties.ElasticStatus.FAILED));
        jsonArray.put(count.get(ElasticProperties.ElasticStatus.OFFLINE));

        jsonArray.put(statistics.getTotalCost());

        parentObject.put(EC2, jsonArray);
    }

    private void addEbsDetails(JSONObject parentObject, Statistics statistics) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(statistics.getDetachedVolumes() >= detachedVolumesThreshold);
        jsonArray.put(statistics.getDetachedVolumes());

        parentObject.put(EBS, jsonArray);
    }
}
