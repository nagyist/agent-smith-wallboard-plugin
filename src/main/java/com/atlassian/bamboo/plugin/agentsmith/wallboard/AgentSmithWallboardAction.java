package com.atlassian.bamboo.plugin.agentsmith.wallboard;

import com.atlassian.bamboo.plugin.agentsmith.AgentSmithService;
import com.atlassian.bamboo.plugin.agentsmith.statistic.QueuedJob;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.opensymphony.webwork.dispatcher.json.JSONException;
import com.opensymphony.webwork.dispatcher.json.JSONObject;

import java.util.concurrent.TimeUnit;

/**
 * Abstract agent smith action which sends the current statistics as a Json object.
 */
public abstract class AgentSmithWallboardAction extends BambooActionSupport {
    /**
     * Default overdue threshold, 1 minute.
     */
    private static final long DEFAULT_QUEUE_OVERDUE_THRESHOLD = TimeUnit.MINUTES.toMillis(1);
    /**
     * Default refresh interval, 15 seconds.
     */
    private static final long DEFAULT_REFRESH_INTERVAL = TimeUnit.SECONDS.toMillis(15);
    private final AgentSmithService agentSmithService;
    private long queueOverdueThreshold = DEFAULT_QUEUE_OVERDUE_THRESHOLD;
    private long refreshInterval = DEFAULT_REFRESH_INTERVAL;

    /**
     * Creates a wallboard action capable of generating statistics in a Json format.
     *
     * @param agentSmithService Service generating statistics.
     */
    public AgentSmithWallboardAction(AgentSmithService agentSmithService) {
        this.agentSmithService = agentSmithService;
    }

    /**
     * Gets the bamboo context to query the Json file.
     *
     * @return the current context of bamboo to query the Json file.
     */
    @SuppressWarnings("unused")
    public String getContext() {
        return getBambooUrl().rootContext();
    }

    /**
     * Gets the overdue threshold for queued items, in milliseconds.
     *
     * @return the overdue threshold for queued items, in milliseconds.
     */
    @SuppressWarnings("unused")
    public long getQueueOverdueThreshold() {
        return queueOverdueThreshold;
    }

    /**
     * Sets the overdue threshold for queued items, in milliseconds.
     *
     * @param queueOverdueThreshold threshold for queued items, in milliseconds.
     */
    @SuppressWarnings("unused")
    public void setQueueOverdueThreshold(long queueOverdueThreshold) {
        this.queueOverdueThreshold = queueOverdueThreshold;
    }

    /**
     * Gets the refresh interval, in milliseconds.
     *
     * @return refresh interval for the javascript script, in milliseconds.
     */
    @SuppressWarnings("unused")
    public long getRefreshInterval() {
        return refreshInterval;
    }

    /**
     * Sets the refresh interval, in milliseconds.
     *
     * @param refreshInterval refresh interval for the javascript script, in milliseconds.
     */
    @SuppressWarnings("unused")
    public void setRefreshInterval(long refreshInterval) {
        this.refreshInterval = refreshInterval;
    }

    /**
     * Sets the number of seconds before the next refresh.
     *
     * @param secondsBeforeNextRefresh number of seconds before the next refresh.
     * @deprecated Use {@link #setRefreshInterval(long)} instead.
     */
    @Deprecated
    @SuppressWarnings("unused")
    public void setSecondsBeforeNextRefresh(long secondsBeforeNextRefresh) {
        setRefreshInterval(TimeUnit.SECONDS.toMillis(secondsBeforeNextRefresh));
    }

    /**
     * Sets the number of minutes before considering a job as overdue.
     *
     * @param minutesBeforeQueueItemOverdue number of minutes before considering a job as overdue.
     * @deprecated Use {@link #setQueueOverdueThreshold(long)} instead.
     */
    @Deprecated
    @SuppressWarnings("unused")
    public void setMinutesBeforeQueueItemOverdue(long minutesBeforeQueueItemOverdue) {
        setQueueOverdueThreshold(TimeUnit.MINUTES.toMillis(minutesBeforeQueueItemOverdue));
    }

    /**
     * Runs the action returning the HTML wallboard page.
     *
     *
     * @return always success.
     */
    @Override
    public String execute() {
        return SUCCESS;
    }

    /**
     * Runs the action returning the Json wallboard page.
     *
     * @return always success.
     */
    @SuppressWarnings("unused")
    public String doJson() {
        return SUCCESS;
    }

    @Override
    public JSONObject getJsonObject() throws JSONException {
        return convertStatistics(agentSmithService.getStatistics());
    }

    /**
     * Converts statistics into a Json object.
     *
     * @param statistics statistics to convert.
     * @return converted object.
     * @throws JSONException when the conversion to Json failed.
     */
    protected abstract JSONObject convertStatistics(Statistics statistics) throws JSONException;

    /**
     * Checks whether a job is considered as overdue or not.
     *
     * @param queuedJob job to test.
     * @return true if the job in in the queue for more than {@code queueOverdueThreshold}.
     */
    public boolean isOverdue(QueuedJob queuedJob) {
        long overdueTimeStamp = queuedJob.getQueuedTime().getTime() + queueOverdueThreshold;
        return overdueTimeStamp > System.currentTimeMillis();
    }
}
