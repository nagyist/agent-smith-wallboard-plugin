package com.atlassian.bamboo.plugin.agentsmith.statistic;

import com.amazonaws.services.ec2.model.Instance;
import com.atlassian.aws.ec2.InstancePaymentType;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Status;
import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Type;
import static com.atlassian.bamboo.plugin.agentsmith.statistic.ElasticProperties.ElasticStatus;

/**
 * Statistics based on the current status of Bamboo.
 */
public class Statistics {
    private final long timestamp;
    private Map<Long, Agent> agents;
    private Map<Agent.Type, List<Agent>> agentsPerType;
    private Map<Agent.Type, Count<Status>> statusCountPerAgentType;
    private Map<InstancePaymentType, List<Agent>> agentsPerPaymentType;
    private Map<InstancePaymentType, Count<Status>> statusCountPerPaymentType;
    private Count<ElasticStatus> elasticStatusCount;
    private Count<Type> dedicatedAgentCount;
    private long elasticTotalWaitingTime;
    private long elasticMaxWaitingTime;
    private double totalCost;
    private List<QueuedJob> queuedJobs;
    private Date longestQueuedDate;
    private long totalQueueTime;
    private long detachedVolumes;

    /**
     * Creates a statistics element.
     */
    public Statistics() {
        this.timestamp = System.currentTimeMillis();
    }

    /**
     * Gets a map of every agent available based on their ID.
     *
     * @return a map of every agent available.
     */
    public Map<Long, Agent> getAgents() {
        return Collections.unmodifiableMap(agents);
    }

    /**
     * Sets the map of agents currently handled by Bamboo and calculate the associated statistics.
     *
     * @param analyzedAgents agents mapped by their unique ID.
     */
    public void analyzeAgents(Map<Long, Agent> analyzedAgents) {
        this.agents = analyzedAgents;

        /*
         * Prepare the statistics per agent type.
         */
        agentsPerType = new HashMap<Type, List<Agent>>();
        statusCountPerAgentType = new HashMap<Type, Count<Status>>();
        for (Type type : Type.values()) {
            agentsPerType.put(type, new LinkedList<Agent>());
            statusCountPerAgentType.put(type, new Count<Status>(Status.class));
        }
        /*
         * Prepare the statistics per payment type (elastic agents only).
         */
        agentsPerPaymentType = new HashMap<InstancePaymentType, List<Agent>>();
        statusCountPerPaymentType = new HashMap<InstancePaymentType, Count<Status>>();
        for (InstancePaymentType instancePaymentType : InstancePaymentType.values()) {
            agentsPerPaymentType.put(instancePaymentType, new LinkedList<Agent>());
            statusCountPerPaymentType.put(instancePaymentType, new Count<Status>(Status.class));
        }

        elasticStatusCount = new Count<ElasticStatus>(ElasticStatus.class);

        dedicatedAgentCount = new Count<Type>(Type.class);

        /*
         * Find the statistics about the agents.
         */
        for (Agent agent : analyzedAgents.values()) {
            Type agentType = agent.getType();
            Status agentStatus = agent.getStatus();

            agentsPerType.get(agentType).add(agent);
            statusCountPerAgentType.get(agentType).increment(agentStatus);

            if(agent.isDedicated()){
                dedicatedAgentCount.increment(agentType);
            }

            if (agent.getType() == Type.ELASTIC) {
                ElasticProperties elasticProperties = agent.getElasticProperties();

                InstancePaymentType agentPaymentType = elasticProperties.getPaymentType();
                agentsPerPaymentType.get(agentPaymentType).add(agent);
                statusCountPerPaymentType.get(agentPaymentType).increment(agentStatus);

                ElasticStatus elasticStatus = elasticProperties.getElasticStatus();
                elasticStatusCount.increment(elasticStatus);

                if (elasticStatus == ElasticStatus.PENDING)
                    analyzeElasticPendingDetails(elasticProperties);

                totalCost += elasticProperties.getCost();
            }
        }
    }

    /**
     * Set the number of disconnected EC2 instances
     * @param disconnectedInstances
     */
    public void analyzeDisconnectedElasticInstances(Collection<Instance> disconnectedInstances)
    {
        for(Instance instance : disconnectedInstances)
        {
            elasticStatusCount.increment(ElasticStatus.DISCONNECTED);
        }
    }

    private void analyzeElasticPendingDetails(ElasticProperties elasticProperties) {
        Date launchTime = elasticProperties.getLaunchTime();
        if (launchTime != null) {
            long waitingTime = System.currentTimeMillis() - launchTime.getTime();
            elasticTotalWaitingTime += waitingTime;
            if (waitingTime > elasticMaxWaitingTime)
                elasticMaxWaitingTime = waitingTime;
        }
    }

    /**
     * Get the list of every queued jobs.
     *
     * @return the list of every queued jobs.
     */
    public List<QueuedJob> getQueuedJobs() {
        return Collections.unmodifiableList(queuedJobs);
    }

    /**
     * Set the queued jobs and calculate the associated statistics.
     *
     * @param analyzedQueuedJobs jobs currently in the building queue.
     */
    public void analyzeQueuedJobs(List<QueuedJob> analyzedQueuedJobs) {
        this.queuedJobs = analyzedQueuedJobs;
        for (QueuedJob queuedJob : analyzedQueuedJobs) {
            Date queuedTime = queuedJob.getQueuedTime();
            if (longestQueuedDate == null || queuedTime.compareTo(longestQueuedDate) > 0)
                longestQueuedDate = queuedTime;
            totalQueueTime += TimeUnit.MILLISECONDS.toSeconds(queuedTime.getTime());
        }
    }

    /**
     * Gets a map of every agent grouped by {@link Type}.
     *
     * @return agents grouped by type.
     */
    public Map<Agent.Type, List<Agent>> getAgentsPerType() {
        return Collections.unmodifiableMap(agentsPerType);
    }

    /**
     * Gets a map of the number of agents per status per agent type.
     *
     * @return number of agents per status per agent type.
     */
    public Map<Agent.Type, Count<Status>> getStatusCountPerAgentType() {
        return Collections.unmodifiableMap(statusCountPerAgentType);
    }

    /**
     * Gets a map of elastic agents grouped by their payment type.
     *
     * @return the elastic agents per payment type
     */
    public Map<InstancePaymentType, List<Agent>> getAgentsPerPaymentType() {
        return Collections.unmodifiableMap(agentsPerPaymentType);
    }

    /**
     * Gets a map of the number dedicated agents per agent type.
     *
     * @return number of dedicated agents per agent type
     */
    public Count<Type> getDedicatedAgentCount() {
        return dedicatedAgentCount;
    }

    /**
     * Gets a map of the number of elastic agents per status per payment type.
     *
     * @return the number of elastic agent per status per payment type.
     */
    public Map<InstancePaymentType, Count<Status>> getStatusCountPerPaymentType() {
        return Collections.unmodifiableMap(statusCountPerPaymentType);
    }

    /**
     * Gets the number of elastic agents with a specific elastic status.
     *
     * @return the number of elastic agents per elastic status.
     */
    public Count<ElasticStatus> getElasticStatusCount() {
        return elasticStatusCount;
    }

    /**
     * Get the total waiting time of all elastic agents in milliseconds.
     *
     * @return total waiting time of elastic agents in milliseconds.
     */
    public long getElasticTotalWaitingTime() {
        return elasticTotalWaitingTime;
    }

    /**
     * Gets the maximum amount of time spent by an elastic agent waiting in milliseconds.
     *
     * @return the maximum amount of time spent by an elastic agent waiting, in milliseconds.
     */
    public long getElasticMaxWaitingTime() {
        return elasticMaxWaitingTime;
    }

    /**
     * Gets the total cost of the elastic agents.
     *
     * @return the total cost of every elastic agents in US dollars.
     */
    public double getTotalCost() {
        return totalCost;
    }

    /**
     * Gets the number of detached volumes.
     *
     * @return the number of detached volumes.
     */
    public long getDetachedVolumes() {
        return detachedVolumes;
    }

    /**
     * Set and calculate statistics based on the detachedVolumes.
     *
     * @param analyzedDetachedVolumes number of detached volumes.
     */
    public void analyzeDetachedVolumes(long analyzedDetachedVolumes) {
        this.detachedVolumes = analyzedDetachedVolumes;
    }

    /**
     * Timestamp in milliseconds of the creation of the statistics.
     *
     * @return timestamp in milliseconds of the creation of the statistics.
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * Gets the queued date of the oldest job in the queue.
     *
     * @return the queued date of the oldest job in the queue.
     */
    public Date getLongestQueuedDate() {
        return longestQueuedDate;
    }

    /**
     * Gets the sum of time spent in the queue of evert job, in seconds.
     *
     * @return the sum of time spent int the queue for every job, in seconds.
     */
    public long getTotalQueueTime() {
        return totalQueueTime;
    }

    /**
     * Counter allowing to keep track of the number of agents with a specific characteristic (enumerated values).
     *
     * @param <E> characteristic on which the count is based (usually an Enum).
     */
    public final class Count<E> implements Iterable<Map.Entry<E, Long>> {
        private final Map<E, MutableLong> counts = new HashMap<E, MutableLong>();

        /**
         * Creates a count system based on a finite collection of items.
         *
         * @param values items for which a counter is made.
         */
        private Count(Iterable<E> values) {
            for (E value : values)
                counts.put(value, new MutableLong());
        }

        /**
         * Creates a count system based on a finite collection of items.
         *
         * @param values items for which a counter is made.
         */
        private Count(E... values) {
            for (E value : values)
                counts.put(value, new MutableLong());
        }

        /**
         * Creates a count system based on an enumeration.
         *
         * @param clazz class of the enumeration.
         */
        private Count(Class<E> clazz) {
            this(Arrays.asList(clazz.getEnumConstants()));
        }

        /**
         * Increments the counter for an element.
         *
         * @param element element for which the counter is incremented.
         */
        private void increment(E element) {
            counts.get(element).increment();
        }

        /**
         * Retrieves the value of the counter for a given element.
         *
         * @param element element for which the counter is retrieved
         * @return the number of time {@link #increment(Object)} has been called for the given element.
         */
        public long get(E element) {
            return counts.get(element).get();
        }

        @Override
        public Iterator<Map.Entry<E, Long>> iterator() {
            final Iterator<Map.Entry<E, MutableLong>> iterator = counts.entrySet().iterator();
            return new Iterator<Map.Entry<E, Long>>() {
                @Override
                public boolean hasNext() {
                    return iterator.hasNext();
                }

                @Override
                public Map.Entry<E, Long> next() {
                    final Map.Entry<E, MutableLong> mutableLongEntry = iterator.next();
                    return new Map.Entry<E, Long>() {

                        @Override
                        public E getKey() {
                            return mutableLongEntry.getKey();
                        }

                        @Override
                        public Long getValue() {
                            return mutableLongEntry.getValue().get();
                        }

                        @Override
                        public Long setValue(Long value) {
                            throw new UnsupportedOperationException();
                        }
                    };
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException();
                }
            };
        }

        /**
         * Wrapper allowing longs to be incremented within a Collection (without reassigning them).
         */
        private class MutableLong {
            private long value = 0;

            private void increment() {
                ++value;
            }

            private long get() {
                return value;
            }
        }
    }
}
