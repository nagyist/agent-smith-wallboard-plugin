package com.atlassian.bamboo.plugin.agentsmith;

import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Volume;
import com.atlassian.aws.AWSAccount;
import com.atlassian.aws.AWSException;
import com.atlassian.aws.ec2.InstancePaymentType;
import com.atlassian.aws.ec2.InstanceStatus;
import com.atlassian.aws.ec2.RemoteEC2Instance;
import com.atlassian.aws.ec2.awssdk.AwsSupportConstants;
import com.atlassian.aws.utils.AwsFunctions;
import com.atlassian.bamboo.agent.elastic.aws.AwsAccountBean;
import com.atlassian.bamboo.agent.elastic.server.ElasticAccountManagementService;
import com.atlassian.bamboo.agent.elastic.server.ElasticInstanceManager;
import com.atlassian.bamboo.agent.elastic.server.RemoteElasticInstance;
import com.atlassian.bamboo.agent.elastic.server.RemoteElasticInstanceState;
import com.atlassian.bamboo.build.BuildExecutionManager;
import com.atlassian.bamboo.buildqueue.ElasticAgentDefinition;
import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Agent;
import com.atlassian.bamboo.plugin.agentsmith.statistic.ElasticProperties;
import com.atlassian.bamboo.plugin.agentsmith.statistic.QueuedJob;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.CurrentlyBuilding;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.queue.BuildQueueManager;
import com.atlassian.bamboo.ww2.actions.admin.elastic.ElasticUIBean;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.*;

import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Status;
import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Type;
import static com.atlassian.bamboo.plugin.agentsmith.statistic.ElasticProperties.ElasticStatus;

/**
 * Default implementation providing statistics on Bamboo.
 */
public class AgentSmithServiceImpl implements AgentSmithService {
    private static final Logger logger = LoggerFactory.getLogger(AgentSmithServiceImpl.class);
    private final AgentManager agentManager;
    private final ElasticInstanceManager elasticInstanceManager;
    private final BuildExecutionManager buildExecutionManager;
    private final BuildQueueManager buildQueueManager;
    private final ElasticUIBean elasticUIBean;
    private final AwsAccountBean awsAccountBean;
    private final ElasticAccountManagementService elasticAccountManagementService;

    /**
     * Creates a statistic service.
     *
     * @param agentManager           Service managing the agents.
     * @param elasticInstanceManager Service managing the elastic agents.
     * @param buildExecutionManager  Service managing the build execution.
     * @param buildQueueManager      Service managing the build queue.
     * @param elasticUIBean          Elastic agents details.
     * @param awsAccountBean         Amazon Web Services account details.
     */
    public AgentSmithServiceImpl(AgentManager agentManager, ElasticInstanceManager elasticInstanceManager,
                                 BuildExecutionManager buildExecutionManager,
                                 BuildQueueManager buildQueueManager, ElasticUIBean elasticUIBean,
                                 AwsAccountBean awsAccountBean, ElasticAccountManagementService elasticAccountManagementService) {
        this.agentManager = agentManager;
        this.elasticInstanceManager = elasticInstanceManager;
        this.buildExecutionManager = buildExecutionManager;
        this.buildQueueManager = buildQueueManager;
        this.elasticUIBean = elasticUIBean;
        this.awsAccountBean = awsAccountBean;
        this.elasticAccountManagementService = elasticAccountManagementService;
    }

    @Override
    public Statistics getStatistics() {
        logger.debug("Starting to gather statistics.");
        Statistics statistics = new Statistics();
        Map<Long, Agent> agents = getAgents();
        statistics.analyzeAgents(agents);
        statistics.analyzeDisconnectedElasticInstances(getDisconnectedAwsInstances(agents.values()));
        statistics.analyzeQueuedJobs(getQueueStatistics());
        statistics.analyzeDetachedVolumes(getDetachedVolumes());
        logger.debug("Finished to gather statistics.");

        return statistics;
    }

    private long getDetachedVolumes() {
        long result = 0;
        try {
            AWSAccount awsAccount = getAwsAccount();
            if (awsAccount != null) {
                for (Volume volume : awsAccount.describeVolumes()) {
                    if (volume.getAttachments().isEmpty())
                        result++;
                }
            }
        } catch (Exception e) {
            logger.error("Couldn't get the number of detached volumes.", e);
        }

        return result;
    }

    private List<QueuedJob> getQueueStatistics() {
        List<QueuedJob> queuedJobs = new LinkedList<QueuedJob>();

        try {
            for (CommonContext commonContext : buildQueueManager.getBuildQueue()) {
                try {
                    CurrentlyBuilding currentlyBuilding =
                            buildExecutionManager.getCurrentlyBuildingByPlanResultKey(commonContext.getResultKey());
                    if (currentlyBuilding == null)
                        continue;

                    String planName = commonContext.getDisplayName();
                    String planKey = commonContext.getResultKey().getKey();
                    Date queueTime = currentlyBuilding.getQueueTime();
                    QueuedJob queuedJob = new QueuedJob(planName, planKey, queueTime);
                    queuedJob.setAgentAvailability(getAgentAvailability(currentlyBuilding));
                    queuedJobs.add(queuedJob);
                } catch (Exception e) {
                    logger.error("Couldn't get statistics of '" + commonContext.getDisplayName() + "'", e);
                }
            }
        } catch (Exception e) {
            logger.error("Couldn't get statistics in the building queue.", e);
        }

        return queuedJobs;
    }

    private QueuedJob.AgentAvailability getAgentAvailability(CurrentlyBuilding currentlyBuilding) {
        if (currentlyBuilding.hasExecutableAgents())
            return QueuedJob.AgentAvailability.EXECUTABLE;
        else if (!currentlyBuilding.getExecutableElasticImages().isEmpty())
            return QueuedJob.AgentAvailability.ELASTIC;
        else
            return QueuedJob.AgentAvailability.NONE;
    }

    /**
     * Retrieves agents and their details.
     *
     * @return agents.
     */
    private Map<Long, Agent> getAgents() {
        Map<Long, Agent> agents = new HashMap<Long, Agent>();

        List<BuildAgent> localAgents = new ArrayList<BuildAgent>(agentManager.getAllLocalAgents());
        List<BuildAgent> remoteAgents = new ArrayList<BuildAgent>(agentManager.getAllRemoteAgents());

        try {
            //TODO: Replace the verification with a check using AdministrationConfigurationAccessor.
            if (getAwsAccount() != null) {
                List<BuildAgent> elasticRunningAgents =
                        new ArrayList<BuildAgent>(agentManager.getOnlineElasticAgents());
                remoteAgents.removeAll(elasticRunningAgents);
                for (Agent agent : createAgentsByType(elasticRunningAgents, Type.ELASTIC)) {
                    agents.put(agent.getId(), agent);
                }
                setElasticDetails(agents);
            }
        } catch (Exception e) {
            logger.error("Couldn't get the statistics on the elastic agents.", e);
        }

        try {
            for (Agent agent : createAgentsByType(remoteAgents, Type.REMOTE)) {
                agents.put(agent.getId(), agent);
            }
        } catch (Exception e) {
            logger.error("Couldn't get the statistics on the remote agents.", e);
        }

        try {
            for (Agent agent : createAgentsByType(localAgents, Type.LOCAL)) {
                agents.put(agent.getId(), agent);
            }
        } catch (Exception e) {
            logger.error("Couldn't get the statistics on the local agents.", e);
        }
        return agents;
    }

    /**
     * Creates a List of agents based on a list of BuildAgents.
     *
     * @param buildAgents Original build agent objects on which the agents objects will be built.
     * @param type        Type of agents.
     * @return the agents.
     */
    private List<Agent> createAgentsByType(List<BuildAgent> buildAgents, Type type) {
        List<Agent> agents = new ArrayList<Agent>(buildAgents.size());

        for (BuildAgent buildAgent : buildAgents) {
            Agent agent = new Agent(buildAgent.getId(), type);

            if (!buildAgent.isActive()) {
                agent.setStatus(Status.OFFLINE);
            } else if (!buildAgent.isEnabled()) {
                agent.setStatus(Status.DISABLED);
            } else if (!buildAgent.isBusy()) {
                agent.setStatus(Status.IDLE);
            } else {
                CurrentlyBuilding currentlyBuilding =
                        buildExecutionManager.getBuildRunningOnAgent(buildAgent.getId());
                if (currentlyBuilding != null && currentlyBuilding.getBuildHangDetails() != null) {
                    agent.setStatus(Status.HUNG);
                } else {
                    agent.setStatus(Status.BUSY);
                }
            }

            // Create a helper to use implementation of BuildAgent.isDedicated(). Because the method is deprecated and there's no replacement.
            // Create a PR to bamboo to add a helper method AgentAssignmentHelper.isDedicatedAgent .
            // Todo add replace with the helper method once available in bamboo
            agent.setDedicated(AgentAssignmentHelper.isDedicatedAgent(buildAgent));
            agents.add(agent);
        }
        return agents;
    }

    /**
     * Retrieves additional data for Elastic agents.
     * <p>
     * {@link Agent} with the {@link Type} {@link Type#ELASTIC} will have their
     * {@link Agent#getElasticProperties()} set properly.
     * </p>
     *
     * @param agents map of every existing agents.
     */
    private void setElasticDetails(Map<Long, Agent> agents) {
        List<RemoteElasticInstance> connectedElasticInstances = elasticInstanceManager.getAllElasticRemoteAgents();

        for (RemoteElasticInstance elasticInstance : connectedElasticInstances) {
            try {
                //TODO: Check that the null condition is indeed unexpected, if not should a new Agent be created?
                Agent agent = agents.get(elasticInstance.getRemoteAgent());
                if (agent == null) {
                    logger.error("The remote agent '{}' is provided by the ElasticInstanceManager"
                            + " but somehow isn't in the standard InstanceManager", elasticInstance.getRemoteAgent());
                    continue;
                }

                ElasticProperties agentProperties = agent.getElasticProperties();

                /*
                 * Find the state.
                 */
                ElasticStatus elasticStatus = getElasticStatus(elasticInstance);
                agentProperties.setElasticStatus(elasticStatus);

                /*
                 * Add the EC2 properties.
                 */
                RemoteEC2Instance remoteEC2Instance = elasticInstance.getInstance();
                addEc2Properties(agentProperties, remoteEC2Instance);

                Double instanceCost = elasticUIBean.getInstancePrice(elasticInstance);
                if (instanceCost != null)
                    agentProperties.setCost(instanceCost);
            } catch (Exception e) {
                logger.error("Couldn't gather the elastic details for '{}'", elasticInstance.getRemoteAgent());
            }
        }
    }

    /**
     * Find out the disconnected EC2 instances
     * @return Collection of disconnected EC2 instances
     */
    public Collection<Instance> getDisconnectedAwsInstances(Collection<Agent> agents) {
        Collection<Instance> result = Sets.newHashSet();
        try {
            result.addAll(elasticAccountManagementService.getDisconnectedElasticInstances());
            result.addAll(elasticAccountManagementService.getUnrelatedElasticInstances());
        }catch (Exception e) {
            logger.error("Failed to get disconnected ", e);
        }

        return result;
    }

    /**
     * Find the EC2 properties of the server on which the agent is running and add them to the elastic agent details.
     *
     * @param agentProperties   agent to update with the EC2 details.
     * @param remoteEC2Instance EC2 instance.
     */
    private void addEc2Properties(ElasticProperties agentProperties, RemoteEC2Instance remoteEC2Instance) {
        if (remoteEC2Instance != null) {
            InstanceStatus instanceStatus = remoteEC2Instance.getInstanceStatus();
            InstancePaymentType paymentType = instanceStatus.getInstancePaymentType();
            agentProperties.setPaymentType(paymentType);
            agentProperties.setElasticInstanceId(remoteEC2Instance.getInstanceId());

            if (agentProperties.getElasticStatus() == ElasticStatus.PENDING && instanceStatus.getLaunchTime() != null) {
                agentProperties.setLaunchTime(instanceStatus.getLaunchTime());
            }
        } else if (agentProperties.getElasticStatus() == ElasticStatus.PENDING) {
            agentProperties.setElasticStatus(ElasticStatus.PENDING_NO_INSTANCE);
        }
    }

    /**
     * Determines the status of an elastic instance for statistic purposes.
     *
     * @param elasticInstance actual status of the elastic instance.
     * @return the elastic status used for statistical purposes.
     */
    private ElasticStatus getElasticStatus(RemoteElasticInstance elasticInstance) {
        ElasticStatus elasticStatus;
        RemoteElasticInstanceState state = elasticInstance.getState();
        switch (state) {
            case RUNNING:
                if (!elasticInstance.isAgentLoading()) {
                    elasticStatus = ElasticStatus.RUNNING;
                    break;
                }
            case INITIAL:
            case BIDDING:
            case STARTING:
            case IDENTIFIED:
                elasticStatus = ElasticStatus.PENDING;
                break;
            case STOPPING:
            case STOPPED:
            case SHUTTING_DOWN:
                elasticStatus = ElasticStatus.SHUTTING_DOWN;
                break;
            case TERMINATED:
            case FAILED_TO_START:
            case UNKNOWN:
                elasticStatus = ElasticStatus.FAILED;
                break;
            default:
                throw new IllegalArgumentException("The remote instance state '" + state + "' isn't handled.");
        }
        return elasticStatus;
    }

    /**
     * Gets the Amazon Web Services account.
     *
     * @return the AWS account or null if it isn't accessible.
     */
    private AWSAccount getAwsAccount() {
        try {
            return awsAccountBean.getAwsAccount();
        } catch (Exception e) {
            logger.info("Unable to get aws info, assuming AWS is disabled");
        }
        return null;
    }
}
