package com.atlassian.bamboo.plugin.agentsmith.statistic;

import com.atlassian.aws.ec2.InstancePaymentType;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;

import static com.atlassian.aws.ec2.InstancePaymentType.*;
import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Type.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class StatisticsTest {
    private Statistics statistics;
    private Map<Long, Agent> agents;
    private Agent elasticAgent1;
    private Agent elasticAgent2;
    private Agent elasticAgent3;
    private Agent remoteAgent1;
    private Agent remoteAgent2;
    private Agent localAgent1;
    private Agent localAgent2;

    @Mocked("currentTimeMillis")
    private System system;

    @BeforeMethod
    public void setUp() throws Exception {
        statistics = new Statistics();
        agents = new HashMap<Long, Agent>();

        elasticAgent1 = new Agent(0, ELASTIC);
        elasticAgent1.setStatus(Agent.Status.OFFLINE);
        elasticAgent1.getElasticProperties().setElasticStatus(ElasticProperties.ElasticStatus.PENDING);
        elasticAgent1.getElasticProperties().setPaymentType(REGULAR);
        // Wed, 03 Apr 2013 18:07:24
        elasticAgent1.getElasticProperties().setLaunchTime(new Date(1364972844000l));
        elasticAgent1.getElasticProperties().setCost(12.32);
        agents.put(elasticAgent1.getId(), elasticAgent1);

        elasticAgent2 = new Agent(1, ELASTIC);
        elasticAgent2.setStatus(Agent.Status.DISABLED);
        elasticAgent2.getElasticProperties().setElasticStatus(ElasticProperties.ElasticStatus.PENDING);
        elasticAgent2.getElasticProperties().setPaymentType(RESERVED);
        // Sat, 10 Nov 2012 18:03:42
        elasticAgent2.getElasticProperties().setLaunchTime(new Date(1352531022000l));
        elasticAgent2.getElasticProperties().setCost(45.12);
        agents.put(elasticAgent2.getId(), elasticAgent2);

        elasticAgent3 = new Agent(2, ELASTIC);
        elasticAgent3.setStatus(Agent.Status.IDLE);
        elasticAgent3.getElasticProperties().setElasticStatus(ElasticProperties.ElasticStatus.RUNNING);
        elasticAgent3.getElasticProperties().setPaymentType(RESERVED);
        elasticAgent3.getElasticProperties().setCost(42.69);
        agents.put(elasticAgent3.getId(), elasticAgent3);

        remoteAgent1 = new Agent(3, REMOTE);
        remoteAgent1.setStatus(Agent.Status.BUSY);
        agents.put(remoteAgent1.getId(), remoteAgent1);
        remoteAgent2 = new Agent(4, REMOTE);
        remoteAgent2.setStatus(Agent.Status.OFFLINE);
        agents.put(remoteAgent2.getId(), remoteAgent2);

        localAgent1 = new Agent(5, LOCAL);
        localAgent1.setStatus(Agent.Status.HUNG);
        agents.put(localAgent1.getId(), localAgent1);
        localAgent2 = new Agent(6, LOCAL);
        localAgent2.setStatus(Agent.Status.IDLE);
        agents.put(localAgent2.getId(), localAgent2);
    }

    @Test
    public void testAddSingleAgent() {
        statistics.analyzeAgents(Collections.singletonMap(localAgent1.getId(), localAgent1));

        final Map<Long, Agent> retrievedAgents = statistics.getAgents();

        assertThat(retrievedAgents, hasEntry(localAgent1.getId(), localAgent1));
        assertThat(retrievedAgents.size(), is(1));
    }

    @Test
    public void testAddMultipleAgents() {
        Map<Long, Agent> agents = new HashMap<Long, Agent>();
        agents.put(localAgent1.getId(), localAgent1);
        agents.put(localAgent2.getId(), localAgent2);

        statistics.analyzeAgents(agents);

        final Map<Long, Agent> retrievedAgents = statistics.getAgents();

        assertThat(retrievedAgents, hasEntry(localAgent1.getId(), localAgent1));
        assertThat(retrievedAgents, hasEntry(localAgent2.getId(), localAgent2));
        assertThat(retrievedAgents.size(), is(2));
    }

    @Test
    public void testAgentsGroupedByType() {
        statistics.analyzeAgents(agents);

        final Map<Agent.Type, List<Agent>> agentsPerType = statistics.getAgentsPerType();

        assertThat(agentsPerType.get(LOCAL), contains(localAgent1, localAgent2));
        assertThat(agentsPerType.get(LOCAL), hasSize(2));
        assertThat(agentsPerType.get(REMOTE), contains(remoteAgent1, remoteAgent2));
        assertThat(agentsPerType.get(REMOTE), hasSize(2));
        assertThat(agentsPerType.get(ELASTIC), contains(elasticAgent1, elasticAgent2, elasticAgent3));
        assertThat(agentsPerType.get(ELASTIC), hasSize(3));
    }

    @Test
    public void testElasticAgentsGroupedByPaymentType() {
        statistics.analyzeAgents(agents);

        final Map<InstancePaymentType, List<Agent>> elasticAgentsPerPaymentType
                = statistics.getAgentsPerPaymentType();

        assertThat(elasticAgentsPerPaymentType.get(REGULAR), contains(elasticAgent1));
        assertThat(elasticAgentsPerPaymentType.get(REGULAR), hasSize(1));
        assertThat(elasticAgentsPerPaymentType.get(RESERVED), contains(elasticAgent2, elasticAgent3));
        assertThat(elasticAgentsPerPaymentType.get(RESERVED), hasSize(2));
        assertThat(elasticAgentsPerPaymentType.get(SPOT), hasSize(0));
    }

    @Test
    public void testCostCalculations() {
        statistics.analyzeAgents(agents);

        assertThat(statistics.getTotalCost(), is(closeTo(100.13, 0.01)));
    }

    @Test
    public void testMaxTimeCalculation() {
        // Thu, 19 Sep 2013 18:01:41
        final long currentTimeStamp = 1379577701000l;
        final long expectedTimeStamp = currentTimeStamp - elasticAgent2.getElasticProperties().getLaunchTime().getTime();
        new NonStrictExpectations() {

            {
                System.currentTimeMillis();
                result = currentTimeStamp;
            }
        };

        statistics.analyzeAgents(agents);

        assertThat(statistics.getElasticMaxWaitingTime(), is(expectedTimeStamp));
    }

    @Test
    public void testTotalWaitingTimeCalculations() {
        // Thu, 19 Sep 2013 18:01:41
        final long currentTimeStamp = 1379577701000l;
        final long expectedWaitingTime = currentTimeStamp - elasticAgent2.getElasticProperties().getLaunchTime().getTime()
                + currentTimeStamp - elasticAgent1.getElasticProperties().getLaunchTime().getTime();
        new NonStrictExpectations() {

            {
                System.currentTimeMillis();
                result = currentTimeStamp;
            }
        };

        statistics.analyzeAgents(agents);

        assertThat(statistics.getElasticTotalWaitingTime(), is(expectedWaitingTime));
    }
}
