package com.atlassian.bamboo.plugin.agentsmith;

import com.atlassian.aws.AWSAccount;
import com.atlassian.aws.ec2.InstancePaymentType;
import com.atlassian.bamboo.agent.elastic.aws.AwsAccountBean;
import com.atlassian.bamboo.agent.elastic.server.ElasticAccountManagementService;
import com.atlassian.bamboo.agent.elastic.server.ElasticInstanceManager;
import com.atlassian.bamboo.agent.elastic.server.RemoteElasticInstance;
import com.atlassian.bamboo.agent.elastic.server.RemoteElasticInstanceState;
import com.atlassian.bamboo.build.BuildExecutionManager;
import com.atlassian.bamboo.build.monitoring.BuildHungDetails;
import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Agent;
import com.atlassian.bamboo.plugin.agentsmith.statistic.ElasticProperties;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;
import com.atlassian.bamboo.v2.build.CurrentlyBuilding;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.queue.BuildQueueManager;
import com.atlassian.bamboo.ww2.actions.admin.elastic.ElasticUIBean;
import com.google.common.collect.ImmutableList;
import mockit.*;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class StatisticsAgentTest {
    private AgentSmithServiceImpl statisticsService;
    @Mocked
    private AgentManager mockAgentManager;
    @Mocked
    private ElasticInstanceManager mockElasticInstanceManager;
    @Mocked
    private BuildExecutionManager mockBuildExecutionManager;
    @Mocked
    private BuildQueueManager mockBuildQueueManager;
    @Mocked
    private ElasticUIBean mockElasticUIBean;
    @Mocked
    private AwsAccountBean mockAwsAccountBean;
    @Mocked
    private AWSAccount mockAwsAccount;
    @Mocked
    private CurrentlyBuilding currentlyBuilding;
    @Mocked
    private BuildHungDetails buildHungDetails;

    @Injectable
    @Cascading
    private BuildAgent mockAgent;
    private long mockAgentId = 1;
    @Injectable
    @Cascading
    private BuildAgent mockAgent2;
    private long mockAgent2Id = 2;
    @Injectable
    @Cascading
    private RemoteElasticInstance mockElasticInstance;
    @Injectable
    @Cascading
    private RemoteElasticInstance mockElasticInstance2;
    @Mocked(stubOutClassInitialization = true) final AgentAssignmentHelper unused = null;
    @Injectable
    private ElasticAccountManagementService elasticAccountManagementService;

    @BeforeMethod
    public void setUp() throws Exception {
        statisticsService = new AgentSmithServiceImpl(mockAgentManager, mockElasticInstanceManager,
                mockBuildExecutionManager, mockBuildQueueManager, mockElasticUIBean, mockAwsAccountBean, elasticAccountManagementService);
        new NonStrictExpectations() {{
            mockAwsAccountBean.getAwsAccount();
            result = mockAwsAccount;
            mockAgentManager.getAllLocalAgents();
            result = Collections.emptyList();
            mockAgentManager.getAllRemoteAgents();
            result = Collections.emptyList();
            mockAgentManager.getOnlineElasticAgents();
            result = Collections.emptyList();

            mockAgent.getId();
            result = mockAgentId;
            mockElasticInstance.getRemoteAgent();
            result = mockAgentId;
            mockElasticInstance.getInstance().getInstanceStatus().getInstancePaymentType();
            result = InstancePaymentType.RESERVED;

            mockAgent2.getId();
            result = mockAgent2Id;
            mockElasticInstance2.getRemoteAgent();
            result = mockAgent2Id;
            mockElasticInstance2.getInstance().getInstanceStatus().getInstancePaymentType();
            result = InstancePaymentType.REGULAR;

            mockElasticInstanceManager.getAllElasticRemoteAgents();
            result = Arrays.asList(mockElasticInstance, mockElasticInstance2);

            mockBuildQueueManager.getBuildQueue();
            result = ImmutableList.of();

            AgentAssignmentHelper.isDedicatedAgent((BuildAgent)any);
            result = false;
        }};
    }

    @Test
    public void testElasticInstancesUntouchedIfAwsOff() throws Exception {
        new NonStrictExpectations() {{
            mockAwsAccountBean.getAwsAccount();
            result = null;
        }};

        statisticsService.getStatistics();

        new Verifications() {{
            mockAgentManager.getOnlineElasticAgents();
            times = 0;
        }};
    }

    @Test
    public void testElasticInstancesRetrievedIfAwsOn() throws Exception {
        statisticsService.getStatistics();

        new Verifications() {{
            mockAgentManager.getOnlineElasticAgents();
        }};
    }

    @Test
    public void testSingleIdleAgent() throws Exception {
        new NonStrictExpectations() {{
            mockAgentManager.getAllLocalAgents();
            result = Collections.singletonList(mockAgent);

            onInstance(mockAgent).isActive();
            result = true;
            onInstance(mockAgent).isEnabled();
            result = true;
            onInstance(mockAgent).isBusy();
            result = false;
        }};

        Statistics statistics = statisticsService.getStatistics();

        Agent result = statistics.getAgents().get(mockAgentId);
        assertThat(statistics.getAgents(), Matchers.hasKey(mockAgentId));
        assertThat(result.getStatus(), is(Agent.Status.IDLE));
    }

    @Test
    public void testSingleOfflineAgent() throws Exception {
        new NonStrictExpectations() {{
            mockAgentManager.getAllLocalAgents();
            result = Collections.singletonList(mockAgent);

            onInstance(mockAgent).isActive();
            result = false;
        }};

        Statistics statistics = statisticsService.getStatistics();

        Agent result = statistics.getAgents().get(mockAgentId);
        assertThat(statistics.getAgents(), Matchers.hasKey(mockAgentId));
        assertThat(result.getStatus(), is(Agent.Status.OFFLINE));
    }

    @Test
    public void testSingleDisabledAgent() throws Exception {
        new NonStrictExpectations() {{
            mockAgentManager.getAllLocalAgents();
            result = Collections.singletonList(mockAgent);


            onInstance(mockAgent).isActive();
            result = true;
            onInstance(mockAgent).isEnabled();
            result = false;
        }};

        Statistics statistics = statisticsService.getStatistics();

        Agent result = statistics.getAgents().get(mockAgentId);
        assertThat(statistics.getAgents(), Matchers.hasKey(mockAgentId));
        assertThat(result.getStatus(), is(Agent.Status.DISABLED));
    }

    @Test
    public void testSingleHungAgent() throws Exception {
        new NonStrictExpectations() {

            {
                mockAgentManager.getAllLocalAgents();
                result = Collections.singletonList(mockAgent);

                onInstance(mockAgent).isActive();
                result = true;
                onInstance(mockAgent).isEnabled();
                result = true;
                onInstance(mockAgent).isBusy();
                result = true;

                mockBuildExecutionManager.getBuildRunningOnAgent(mockAgentId);
                result = currentlyBuilding;
                currentlyBuilding.getBuildHangDetails();
                result = buildHungDetails;
            }
        };

        Statistics statistics = statisticsService.getStatistics();

        Agent result = statistics.getAgents().get(mockAgentId);
        assertThat(statistics.getAgents(), Matchers.hasKey(mockAgentId));
        assertThat(result.getStatus(), is(Agent.Status.HUNG));
    }

    @Test
    public void testSingleBusyAgent() throws Exception {
        new NonStrictExpectations() {{
            mockAgentManager.getAllLocalAgents();
            result = Collections.singletonList(mockAgent);

            onInstance(mockAgent).isActive();
            result = true;
            onInstance(mockAgent).isEnabled();
            result = true;
            onInstance(mockAgent).isBusy();
            result = true;

            mockBuildExecutionManager.getBuildRunningOnAgent(mockAgentId);
            result = null;
        }};

        Statistics statistics = statisticsService.getStatistics();

        Agent result = statistics.getAgents().get(mockAgentId);
        assertThat(statistics.getAgents(), Matchers.hasKey(mockAgentId));
        assertThat(result.getStatus(), is(Agent.Status.BUSY));
    }

    @Test
    public void testElasticAgentsNotInRemote() throws Exception {
        new NonStrictExpectations() {{
            mockAgentManager.getAllRemoteAgents();
            result = Arrays.asList(mockAgent, mockAgent2);
            mockAgentManager.getOnlineElasticAgents();
            result = Collections.singletonList(mockAgent2);
        }};

        Statistics statistics = statisticsService.getStatistics();

        Agent expectedRemoteAgent = statistics.getAgents().get(mockAgentId);
        assertThat(statistics.getAgentsPerType().get(Agent.Type.REMOTE), hasSize(1));
        assertThat(statistics.getAgentsPerType().get(Agent.Type.REMOTE), contains(expectedRemoteAgent));
    }

    @Test
    public void testElasticStatusRunning() throws Exception {
        new NonStrictExpectations() {{
            mockAgentManager.getOnlineElasticAgents();
            result = Collections.singletonList(mockAgent);
            mockElasticInstance.getState();
            result = RemoteElasticInstanceState.RUNNING;
            mockElasticInstance.isAgentLoading();
            result = false;
        }};

        Statistics statistics = statisticsService.getStatistics();

        Agent elasticAgent = statistics.getAgents().get(mockAgentId);
        assertThat(elasticAgent.getElasticProperties().getElasticStatus(), is(ElasticProperties.ElasticStatus.RUNNING));
    }

    @DataProvider(name = "elasticInstancesStatusForPending")
    private Object[][] elasticInstancesStatusForPending() {
        return new Object[][]{
                {RemoteElasticInstanceState.RUNNING},
                {RemoteElasticInstanceState.INITIAL},
                {RemoteElasticInstanceState.BIDDING},
                {RemoteElasticInstanceState.STARTING},
                {RemoteElasticInstanceState.IDENTIFIED}};
    }

    @Test(dataProvider = "elasticInstancesStatusForPending")
    public void testElasticStatusPending(final RemoteElasticInstanceState remoteElasticInstanceState) throws Exception {
        new NonStrictExpectations() {{
            mockAgentManager.getOnlineElasticAgents();
            result = Collections.singletonList(mockAgent);
            mockElasticInstance.getState();
            result = remoteElasticInstanceState;
            //To test RUNNING state
            mockElasticInstance.isAgentLoading();
            result = true;
        }};

        Statistics statistics = statisticsService.getStatistics();

        Agent elasticAgent = statistics.getAgents().get(mockAgentId);
        assertThat(elasticAgent.getElasticProperties().getElasticStatus(), is(ElasticProperties.ElasticStatus.PENDING));
    }

    @Test
    public void testElasticStatusPendingNoInstance() throws Exception {
        final RemoteElasticInstanceState remoteElasticInstanceState = RemoteElasticInstanceState.BIDDING;
        new NonStrictExpectations() {{
            mockAgentManager.getOnlineElasticAgents();
            result = Collections.singletonList(mockAgent);
            mockElasticInstance.getState();
            result = remoteElasticInstanceState;
            mockElasticInstance.getInstance();
            result = null;
        }};

        Statistics statistics = statisticsService.getStatistics();

        Agent elasticAgent = statistics.getAgents().get(mockAgentId);
        assertThat(elasticAgent.getElasticProperties().getElasticStatus(),
                is(ElasticProperties.ElasticStatus.PENDING_NO_INSTANCE));
    }

    @DataProvider(name = "elasticInstancesStatusForShutting")
    private Object[][] elasticInstancesStatusForShutting() {
        return new Object[][]{
                {RemoteElasticInstanceState.STOPPED},
                {RemoteElasticInstanceState.STOPPING},
                {RemoteElasticInstanceState.SHUTTING_DOWN}};
    }

    @Test(dataProvider = "elasticInstancesStatusForShutting")
    public void testElasticStatusShutting(final RemoteElasticInstanceState remoteElasticInstanceState) throws Exception {
        new NonStrictExpectations() {{
            mockAgentManager.getOnlineElasticAgents();
            result = Collections.singletonList(mockAgent);
            mockElasticInstance.getState();
            result = remoteElasticInstanceState;
        }};

        Statistics statistics = statisticsService.getStatistics();

        Agent elasticAgent = statistics.getAgents().get(mockAgentId);
        assertThat(elasticAgent.getElasticProperties().getElasticStatus(), is(ElasticProperties.ElasticStatus.SHUTTING_DOWN));
    }

    @DataProvider(name = "elasticInstancesStatusForFailed")
    private Object[][] elasticInstancesStatusForFailed() {
        return new Object[][]{
                {RemoteElasticInstanceState.FAILED_TO_START},
                {RemoteElasticInstanceState.TERMINATED},
                {RemoteElasticInstanceState.UNKNOWN}};
    }

    @Test(dataProvider = "elasticInstancesStatusForFailed")
    public void testElasticStatusFailed(final RemoteElasticInstanceState remoteElasticInstanceState) throws Exception {
        new NonStrictExpectations() {{
            mockAgentManager.getOnlineElasticAgents();
            result = Collections.singletonList(mockAgent);
            mockElasticInstance.getState();
            result = remoteElasticInstanceState;
        }};

        Statistics statistics = statisticsService.getStatistics();

        Agent elasticAgent = statistics.getAgents().get(mockAgentId);
        assertThat(elasticAgent.getElasticProperties().getElasticStatus(), is(ElasticProperties.ElasticStatus.FAILED));
    }

    @Test
    public void testPendingLaunchTime() throws Exception {
        // Thu, 19 Sep 2013 18:01:41
        final Date date = new Date(1379577701000l);
        new NonStrictExpectations() {{
            mockAgentManager.getOnlineElasticAgents();
            result = Collections.singletonList(mockAgent);
            mockElasticInstance.getState();
            result = RemoteElasticInstanceState.STARTING;
            mockElasticInstance.getInstance().getInstanceStatus().getLaunchTime();
            result = date;
        }};

        Statistics statistics = statisticsService.getStatistics();

        Agent elasticAgent = statistics.getAgents().get(mockAgentId);
        assertThat(elasticAgent.getElasticProperties().getLaunchTime(), is(date));
    }

    @Test
    public void testElasticCost() throws Exception {
        final double costAgent1 = 1337.42;
        final double costAgent2 = 1242.13;
        new NonStrictExpectations() {{
            mockAgentManager.getOnlineElasticAgents();
            result = Arrays.asList(mockAgent, mockAgent2);
            mockElasticUIBean.getInstancePrice(mockElasticInstance);
            result = costAgent1;
            mockElasticInstance.getState();
            result = RemoteElasticInstanceState.STARTING;
            mockElasticUIBean.getInstancePrice(mockElasticInstance2);
            result = costAgent2;
            mockElasticInstance2.getState();
            result = RemoteElasticInstanceState.STARTING;
        }};

        Statistics statistics = statisticsService.getStatistics();

        assertThat(statistics.getTotalCost(), is(2579.55));
    }

    @Test
    public void testDedicatedRemoteInstance()
    {
        new NonStrictExpectations(){{
            mockAgentManager.getOnlineElasticAgents();
            result = Arrays.asList(mockAgent2);
            mockAgentManager.getAllRemoteAgents();
            result = Arrays.asList(mockAgent);
            AgentAssignmentHelper.isDedicatedAgent((BuildAgent)any);
            result = true;
        }};

        Statistics statistics = statisticsService.getStatistics();
        assertThat(statistics.getDedicatedAgentCount().get(Agent.Type.REMOTE), is(1l));
        assertThat(statistics.getDedicatedAgentCount().get(Agent.Type.ELASTIC), is(1l));
    }
}
