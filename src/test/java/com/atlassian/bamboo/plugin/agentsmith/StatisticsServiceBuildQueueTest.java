package com.atlassian.bamboo.plugin.agentsmith;

import com.atlassian.bamboo.ResultKey;
import com.atlassian.bamboo.agent.elastic.aws.AwsAccountBean;
import com.atlassian.bamboo.agent.elastic.server.ElasticAccountManagementService;
import com.atlassian.bamboo.agent.elastic.server.ElasticImageConfiguration;
import com.atlassian.bamboo.agent.elastic.server.ElasticInstanceManager;
import com.atlassian.bamboo.build.BuildExecutionManager;
import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.plugin.agentsmith.statistic.QueuedJob;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.CurrentlyBuilding;
import com.atlassian.bamboo.v2.build.queue.BuildQueueManager;
import com.atlassian.bamboo.ww2.actions.admin.elastic.ElasticUIBean;
import com.google.common.collect.ImmutableList;
import mockit.Injectable;
import mockit.Mocked;
import mockit.NonStrictExpectations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Date;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class StatisticsServiceBuildQueueTest {
    private AgentSmithServiceImpl statisticsService;
    @Mocked
    private BuildExecutionManager mockBuildExecutionManager = null;
    @Mocked
    private BuildQueueManager mockBuildQueueManager = null;
    @Mocked
    private AgentManager mockAgentManager;
    @Mocked
    private ElasticInstanceManager mockElasticInstanceManager;
    @Mocked
    private ElasticUIBean mockElasticUIBean;
    @Mocked
    private AwsAccountBean mockAwsAccountBean;

    @Injectable
    private CurrentlyBuilding mockCurrentlyBuilding;
    @Injectable
    private CommonContext mockCommonContext = null;
    @Injectable
    private ResultKey mockResultKey = null;
    @Injectable
    private ElasticAccountManagementService elasticAccountManagementService = null;
    @Injectable
    private ElasticImageConfiguration elasticImageConfiguration;

    @BeforeMethod
    public void setUp() throws Exception {
        new NonStrictExpectations() {

            {
                statisticsService = new AgentSmithServiceImpl(mockAgentManager, mockElasticInstanceManager,
                        mockBuildExecutionManager, mockBuildQueueManager, mockElasticUIBean, mockAwsAccountBean, elasticAccountManagementService);

                mockAgentManager.getAllLocalAgents();
                result = Collections.emptyList();
                mockAgentManager.getAllRemoteAgents();
                result = Collections.emptyList();
                mockAgentManager.getOnlineElasticAgents();
                result = Collections.emptyList();

                mockBuildQueueManager.getBuildQueue();
                result = ImmutableList.of();


            }
        };
    }

    @Test
    public void testCurrentlyBuildingIsNull() throws Exception {
        new NonStrictExpectations() {{
            mockBuildQueueManager.getBuildQueue();
            result = ImmutableList.of(mockCommonContext);
            mockBuildExecutionManager.getCurrentlyBuildingByPlanResultKey((ResultKey) any);
            result = null;
        }};

        Statistics statistics = statisticsService.getStatistics();

        assertThat(statistics.getQueuedJobs(), is(empty()));
    }

    @Test
    public void testQueuedJobHasAgentsAvailable() throws Exception {
        final String name = UUID.randomUUID().toString();
        final String key = UUID.randomUUID().toString();
        final Date queuedTime = new Date();
        new NonStrictExpectations() {

            {
                mockBuildQueueManager.getBuildQueue();
                result = ImmutableList.of(mockCommonContext);
                mockBuildExecutionManager.getCurrentlyBuildingByPlanResultKey((ResultKey) any);
                result = mockCurrentlyBuilding;
                mockCommonContext.getDisplayName();
                result = name;
                mockCommonContext.getResultKey();
                result = mockResultKey;
                mockResultKey.getKey();
                result = key;
                mockCurrentlyBuilding.getQueueTime();
                result = queuedTime;
                mockCurrentlyBuilding.hasExecutableAgents();
                result = true;
            }
        };

        Statistics statistics = statisticsService.getStatistics();

        QueuedJob queuedJob = statistics.getQueuedJobs().get(0);
        assertThat(statistics.getQueuedJobs(), hasSize(1));
        assertThat(queuedJob.getAgentAvailability(), is(QueuedJob.AgentAvailability.EXECUTABLE));
        assertThat(queuedJob.getName(), is(name));
        assertThat(queuedJob.getKey(), is(key));
        assertThat(queuedJob.getQueuedTime(), is(queuedTime));
    }

    @Test
    public void testQueuedJobHasElasticAgentsAvailable() throws Exception {
        final String name = UUID.randomUUID().toString();
        final String key = UUID.randomUUID().toString();
        final Date queuedTime = new Date();
        new NonStrictExpectations() {

            {
                mockBuildQueueManager.getBuildQueue();
                result = ImmutableList.of(mockCommonContext);
                mockBuildExecutionManager.getCurrentlyBuildingByPlanResultKey((ResultKey) any);
                result = mockCurrentlyBuilding;
                mockCommonContext.getDisplayName();
                result = name;
                mockCommonContext.getResultKey();
                result = mockResultKey;
                mockResultKey.getKey();
                result = key;
                mockCurrentlyBuilding.getQueueTime();
                result = queuedTime;
                mockCurrentlyBuilding.hasExecutableAgents();
                result = false;
                mockCurrentlyBuilding.getExecutableElasticImages();
                result = Collections.singleton(elasticImageConfiguration);
            }
        };

        Statistics statistics = statisticsService.getStatistics();

        QueuedJob queuedJob = statistics.getQueuedJobs().get(0);
        assertThat(statistics.getQueuedJobs(), hasSize(1));
        assertThat(queuedJob.getAgentAvailability(), is(QueuedJob.AgentAvailability.ELASTIC));
        assertThat(queuedJob.getName(), is(name));
        assertThat(queuedJob.getQueuedTime(), is(queuedTime));
    }

    @Test
    public void testQueuedJobHasNoAgentsAvailable() throws Exception {
        final String name = UUID.randomUUID().toString();
        final String key = UUID.randomUUID().toString();
        final Date queuedTime = new Date();
        new NonStrictExpectations() {

            {
                mockBuildQueueManager.getBuildQueue();
                result = ImmutableList.of(mockCommonContext);
                mockBuildExecutionManager.getCurrentlyBuildingByPlanResultKey((ResultKey) any);
                result = mockCurrentlyBuilding;
                mockCommonContext.getDisplayName();
                result = name;
                mockCommonContext.getResultKey();
                result = mockResultKey;
                mockResultKey.getKey();
                result = key;
                mockCurrentlyBuilding.getQueueTime();
                result = queuedTime;
                mockCurrentlyBuilding.hasExecutableAgents();
                result = false;
                mockCurrentlyBuilding.getExecutableElasticImages();
                result = Collections.emptyList();
            }
        };

        Statistics statistics = statisticsService.getStatistics();

        QueuedJob queuedJob = statistics.getQueuedJobs().get(0);
        assertThat(statistics.getQueuedJobs(), hasSize(1));
        assertThat(queuedJob.getAgentAvailability(), is(QueuedJob.AgentAvailability.NONE));
        assertThat(queuedJob.getName(), is(name));
        assertThat(queuedJob.getQueuedTime(), is(queuedTime));
    }
}
