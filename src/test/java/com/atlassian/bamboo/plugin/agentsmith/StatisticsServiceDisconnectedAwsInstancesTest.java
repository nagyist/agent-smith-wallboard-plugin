package com.atlassian.bamboo.plugin.agentsmith;

import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceState;
import com.atlassian.aws.AWSAccount;
import com.atlassian.aws.AWSException;
import com.atlassian.aws.ec2.RemoteEC2Instance;
import com.atlassian.bamboo.agent.elastic.aws.AwsAccountBean;
import com.atlassian.bamboo.agent.elastic.server.ElasticAccountManagementService;
import com.atlassian.bamboo.agent.elastic.server.ElasticInstanceManager;
import com.atlassian.bamboo.agent.elastic.server.RemoteElasticInstance;
import com.atlassian.bamboo.build.BuildExecutionManager;
import com.atlassian.bamboo.buildqueue.ElasticAgentDefinition;
import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Agent;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.queue.BuildQueueManager;
import com.atlassian.bamboo.ww2.actions.admin.elastic.ElasticUIBean;
import mockit.Injectable;
import mockit.NonStrictExpectations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static de.regnis.q.sequence.core.QSequenceAssert.assertEquals;
import static de.regnis.q.sequence.core.QSequenceAssert.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;

public class StatisticsServiceDisconnectedAwsInstancesTest {
    private AgentSmithServiceImpl statisticsService;
    @Injectable
    private BuildExecutionManager mockBuildExecutionManager;
    @Injectable
    private BuildQueueManager mockBuildQueueManager;
    @Injectable
    private CommonContext mockCommonContext;
    @Injectable
    private AgentManager mockAgentManager;
    @Injectable
    private ElasticInstanceManager mockElasticInstanceManager;
    @Injectable
    private ElasticUIBean mockElasticUIBean;
    @Injectable
    private AwsAccountBean mockAwsAccountBean;
    @Injectable
    private AWSAccount mockAwsAccount;
    @Injectable
    Instance instance1;
    @Injectable
    Instance instance2;
    @Injectable
    Instance instance3;
    @Injectable
    InstanceState state;
    @Injectable
    ElasticAgentDefinition elasticAgentDefinition1;
    @Injectable
    ElasticAgentDefinition elasticAgentDefinition2;
    @Injectable
    BuildAgent agent1;
    @Injectable
    RemoteElasticInstance remoteElasticInstance1;
    @Injectable
    RemoteElasticInstance remoteElasticInstance2;
    @Injectable
    RemoteEC2Instance remoteEC2Instance1;
    @Injectable
    RemoteEC2Instance remoteEC2Instance2;
    @Injectable
    ElasticAccountManagementService mockElasticAccountManagementService;



    @BeforeMethod
    public void setUp() throws Exception {
        statisticsService = new AgentSmithServiceImpl(mockAgentManager, mockElasticInstanceManager,
                mockBuildExecutionManager, mockBuildQueueManager, mockElasticUIBean, mockAwsAccountBean, mockElasticAccountManagementService);
        new NonStrictExpectations(){{
            mockAwsAccountBean.getAwsAccount();
            result = mockAwsAccount;
        }};
    }

    @Test
    public void disconnectedInstancesEmptyIfNoInstance() throws Exception {
        new NonStrictExpectations(){{
            mockAwsAccount.getAllInstances();
            result = Collections.emptyList();
            mockAgentManager.getAllElasticAgentDefinitions((Collection<String>) any);
            result = Collections.emptyList();
            mockElasticAccountManagementService.getDisconnectedElasticInstances();
            result = Collections.emptyList();
            mockElasticAccountManagementService.getUnrelatedElasticInstances();
            result = Collections.emptyList();
        }};

        Collection<Instance> instances = statisticsService.getDisconnectedAwsInstances(Collections.<Agent>emptySet());

        assertThat(instances, is(empty()));
    }

    @Test
    public void disconnectedInstances() throws Exception {
        new NonStrictExpectations(){{
            mockAwsAccount.getAllInstances();
            result = Arrays.asList(instance1, instance2, instance3);
            instance1.getInstanceId();
            result = "1";
            instance1.getState();
            result = state;
            instance2.getInstanceId();
            result = "2";
            instance2.getState();
            result = state;
            instance3.getInstanceId();
            result = "3";
            instance3.getState();
            result = state;
            state.getName();
            result = "running";

            mockElasticAccountManagementService.getDisconnectedElasticInstances();
            result = Arrays.asList(instance2, instance1);
            mockElasticAccountManagementService.getUnrelatedElasticInstances();
            result = Arrays.asList(instance1, instance3);

        }};

        Collection<Instance> instances = statisticsService.getDisconnectedAwsInstances(Collections.<Agent>emptySet());
        assertEquals(3, instances.size());
        assertTrue(instances.contains(instance1));
        assertTrue(instances.contains(instance2));
        assertTrue(instances.contains(instance3));
    }

    @Test
    public void disconnectedEmptyIfException() throws Exception {
        new NonStrictExpectations(){{
            mockElasticAccountManagementService.getDisconnectedElasticInstances();
            result = new AWSException("");
        }};

        Collection<Instance> instances = statisticsService.getDisconnectedAwsInstances(Collections.<Agent>emptySet());

        assertThat(instances, is(empty()));
    }
}
