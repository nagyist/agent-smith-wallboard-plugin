package com.atlassian.bamboo.plugin.agentsmith.statistic;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class AgentTest {
    @Test
    public void testElasticAgentHasElasticProperties() throws Exception {
        final long id = 0;
        Agent agent = new Agent(id, Agent.Type.ELASTIC);

        assertThat(agent.getElasticProperties(), is(notNullValue()));
    }

    @DataProvider(name = "nonElasticAgents")
    private Object[][] nonElasticAgents() {
        return new Object[][]{
                {new Agent(0, Agent.Type.LOCAL)},
                {new Agent(0, Agent.Type.REMOTE)}};
    }

    @Test(dataProvider = "nonElasticAgents", expectedExceptions = UnsupportedOperationException.class)
    public void testNonElasticAgentDoesNotHaveElasticProperties(Agent agent) throws Exception {
        agent.getElasticProperties();
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testAgentTypeNotProvided() throws Exception {
        new Agent(0, null);
    }
}
